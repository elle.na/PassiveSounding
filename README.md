For any doubts, contact me at edonini@fbk.eu :)


Code to identify the list of sharad radargrams in the right gemetrical configuration for passive sounding and also direct sounding conisdering the sun and juppiter.

Passive configuration:
MRO <- - - - MARS <----- JUPPITER/SUN 

Direct configuration:
MARS <- - - - MRO <----- JUPPITER/SUN 


Also, there is a preliminary work on solar burst type 3 detection considering a set of fuzzy rules. 
Intersecating the solar burst time with the candidate radargrams in the direct configuration, one can extract the list of radargrams potentially having a solar burst.




Run: 
- main_multiple_MRO_kernels.m to perform the serach on all the MRO kernels.
- main_rdr2info.m to search for the information of a specific radargram.
- main_top10.m to search in the list of candidate for those having the most powerful sular bust.


Credits:
- SPICE code (license mice folder) from NAIF/JPL/NASA
https://naif.jpl.nasa.gov/naif/toolkit.html
- IDL Toolbox from Charles Pelizzari (see license in the idl folder)


