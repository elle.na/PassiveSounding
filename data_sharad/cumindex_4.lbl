PDS_VERSION_ID             = PDS3
RECORD_TYPE                = FIXED_LENGTH
RECORD_BYTES               = 329
FILE_RECORDS               = 30027
^INDEX_TABLE               = "CUMINDEX.TAB"
VOLUME_ID                  = "MROSH_0004"
DATA_SET_ID                = "MRO-M-SHARAD-3-EDR-V1.0"
INSTRUMENT_HOST_NAME       = "MARS RECONNAISSANCE ORBITER"
INSTRUMENT_NAME            = "SHALLOW RADAR"

OBJECT                     = INDEX_TABLE
  INTERCHANGE_FORMAT       = ASCII
  ROW_BYTES                = 329  
  ROWS                     = 30027
  COLUMNS                  = 20
  INDEX_TYPE               = CUMULATIVE

  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 1
    NAME                   = VOLUME_ID
    DATA_TYPE              = CHARACTER
    START_BYTE             = 2
    BYTES                  = 10
    DESCRIPTION            = "Unique ID for a data volume." 
  END_OBJECT               = COLUMN
  
  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 2
    NAME                   = RELEASE_ID
    DATA_TYPE              = CHARACTER
    START_BYTE             = 15
    BYTES                  = 4
    DESCRIPTION            = "ID of the MRO release to PDS in which 
                              the data product was first included." 
  END_OBJECT               = COLUMN
  
  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 3
    NAME                   = FILE_SPECIFICATION_NAME
    DATA_TYPE              = CHARACTER
    START_BYTE             = 22
    BYTES                  = 59
    DESCRIPTION            = "The full name of a file, including a path name,
                              relative to a PDS volume.  It excludes node or
                              volume location." 
  END_OBJECT               = COLUMN

  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 4
    NAME                   = PRODUCT_ID
    DATA_TYPE              = CHARACTER
    START_BYTE             = 84
    BYTES                  = 24
    DESCRIPTION            = "A permanent, unique ID assigned to a
                              data product by its producer."
  END_OBJECT               = COLUMN
  
  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 5
    NAME                   = PRODUCT_CREATION_TIME
    DATA_TYPE              = TIME
    START_BYTE             = 110
    BYTES                  = 21
    DESCRIPTION            = "Date and time at which data product file was 
                              created, in PDS date-time format (i.e. YYYY-
                              DDDThh:mm:ss.mmm)."
  END_OBJECT               = COLUMN
  
  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 6
    NAME                   = PRODUCT_VERSION_ID
    DATA_TYPE              = CHARACTER
    START_BYTE             = 133
    BYTES                  = 1
    DESCRIPTION            = "Revision status of this product (e.g. A, B, C,
                              ...)."
  END_OBJECT               = COLUMN
  
  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 7
    NAME                   = PRODUCT_VERSION_TYPE
    DATA_TYPE              = CHARACTER
    START_BYTE             = 137
    BYTES                  = 13
    DESCRIPTION            = "Version of an individual data product that 
                              might appear in several incarnations."
  END_OBJECT               = COLUMN  

  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 8
    NAME                   = PRODUCT_TYPE
    DATA_TYPE              = CHARACTER
    START_BYTE             = 153
    BYTES                  = 3
    DESCRIPTION            = "Type or category of a data product within a
                              data set."
  END_OBJECT               = COLUMN
  
  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 9
    NAME                   = MISSION_PHASE_NAME
    DATA_TYPE              = CHARACTER
    START_BYTE             = 159
    BYTES                  = 22
    DESCRIPTION            = "Commonly-used name of a mission phase."
  END_OBJECT               = COLUMN
    
  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 10
    NAME                   = ORBIT_NUMBER
    DATA_TYPE              = ASCII_INTEGER
    START_BYTE             = 183
    BYTES                  = 6
    DESCRIPTION            = "Number of the orbital revolution of the space-
                              craft around a target body."
  END_OBJECT               = COLUMN

  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 11
    NAME                   = START_TIME
    DATA_TYPE              = TIME
    START_BYTE             = 190
    BYTES                  = 21
    DESCRIPTION            = "Date and time of acquistion of the first echo 
                              in the Data Product in UTC system format."
  END_OBJECT               = COLUMN
    
  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 12
    NAME                   = STOP_TIME
    DATA_TYPE              = TIME
    START_BYTE             = 212
    BYTES                  = 21
    DESCRIPTION            = "Date and time of acquisition of th elast echo 
                              in the Data Product in UTC system format."
  END_OBJECT               = COLUMN
  
  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 13
    NAME                   = SPACECRAFT_CLOCK_START_COUNT
    DATA_TYPE              = CHARACTER
    START_BYTE             = 235
    BYTES                  = 18
    DESCRIPTION            = "Value of the spacecraft clock at the time of 
                              acquisition of the first echo in the Data 
                              Product."
  END_OBJECT               = COLUMN

  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 14
    NAME                   = SPACECRAFT_CLOCK_STOP_COUNT
    DATA_TYPE              = CHARACTER
    START_BYTE             = 256
    BYTES                  = 18
    DESCRIPTION            = "Value of the spacecraft clock at the time of 
                              acquisition of the last echo in the Data 
                              Product."
  END_OBJECT               = COLUMN

  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 15
    NAME                   = "MRO:START_SUB_SPACECRAFT_LATITUDE"
    DATA_TYPE              = ASCII_REAL
    START_BYTE             = 276
    BYTES                  = 9
    FORMAT                 = "F9.5"
    DESCRIPTION            = "Planetocentric latitude of the sub-spacecraft
                              point at the beginning of data collection."
  END_OBJECT               = COLUMN
    
  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 16
    NAME                   = "MRO:STOP_SUB_SPACECRAFT_LATITUDE"
    DATA_TYPE              = ASCII_REAL
    START_BYTE             = 286
    BYTES                  = 9
    FORMAT                 = "F9.5"
    DESCRIPTION            = "Planetocentric latitude of the sub-spacecraft
                              point at the end of data collection."
  END_OBJECT               = COLUMN

  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 17
    NAME                   = "MRO:START_SUB_SPACECRAFT_LONGITUDE"
    DATA_TYPE              = ASCII_REAL
    START_BYTE             = 296
    BYTES                  = 10
    FORMAT                 = "F10.6"
    DESCRIPTION            = "Planetocentric east longitude of the sub-
                              spacecraft point at the beginning of data 
                              collection."
  END_OBJECT               = COLUMN
  
  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 18
    NAME                   = "MRO:STOP_SUB_SPACECRAFT_LONGITUDE"
    DATA_TYPE              = ASCII_REAL
    START_BYTE             = 307
    BYTES                  = 10
    FORMAT                 = "F10.6"
    DESCRIPTION            = "Planetocentric east longitude of the sub-
                              spacecraft point at the end of data 
                              collection."
  END_OBJECT               = COLUMN

  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 19
    NAME                   = INSTRUMENT_MODE_ID
    DATA_TYPE              = CHARACTER
    START_BYTE             = 319
    BYTES                  = 4
    DESCRIPTION            = "Instrument-dependent designation of operating
                              mode."
  END_OBJECT               = COLUMN
  
  OBJECT                   = COLUMN
    COLUMN_NUMBER          = 20
    NAME                   = DATA_QUALITY_ID
    DATA_TYPE              = CHARACTER
    START_BYTE             = 326
    BYTES                  = 1
    DESCRIPTION            = "Numeric key which identifies the quality of 
                              data in the data product."
  END_OBJECT               = COLUMN

END_OBJECT                 = INDEX_TABLE
END
