function [idx_plt,diff_zx_rotated_deg_idx_plt] = find_index_theta_star_rough(diff_zx_rotated, time_UTC, time_interval,fl_plot,delta_theta_star)

load('C:\Users\admin\Desktop\GIT\PassiveSounding\spec_theta(h).mat');
theta_sc_star = double(theta_sc_star);

%% rough calculation

diff_zx_rotated_deg = diff_zx_rotated;

theta_sc_star_deg = double(rad2deg(theta_sc_star));
th_min = min(theta_sc_star_deg(:))- delta_theta_star;
th_max = max(theta_sc_star_deg(:))+delta_theta_star;

[idx] = find(diff_zx_rotated_deg <= th_max);
tmp=diff_zx_rotated_deg(idx);
[idx2] = find(tmp >= th_min);
diff_zx_rotated_deg_idx_plt = tmp(idx2);
idx_plt = idx(idx2);

if fl_plot==1
    figure; 
    
    plot(diff_zx_rotated_deg);
    hold on
    plot(idx_plt, diff_zx_rotated_deg_idx_plt,'r.');

    hold off
    
    title(['Angular separation on the rotated zx plane (colatitude) from ' time_UTC{1} ' to ' time_UTC{end}]);
    xlabel(['Time with steps of ' num2str(time_interval) ' s']);
    ylabel('Colatitude in deg')

end


end