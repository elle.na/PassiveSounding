function plot3d_planets_MRO_STEREO( date_MRO )

frame    = 'IAU_SUN';
abcorr   = 'LT+S';
observer = 'SUN';

[et] = cspice_str2et( datestr(date_MRO));


% Mars
[ state_source_rec_mars ] = cspice_spkpos( 'MARS', et, frame, abcorr, observer); %km
% [ state_source_rec ] = cspice_spkezr( target_source, time_array, frame, abcorr, observer);%km

[~, colat, lon] = cspice_recsph(state_source_rec_mars);%rad
%state_source_sph = [r; colat; lon]; %rad

mars_rect = cspice_sphrec(3 , colat, lon);
clear r colat lon

%%%% stereo
target_sc   = 'STEREO Ahead';
[ state_rect_stereo ] = cspice_spkpos( target_sc, et, frame, abcorr, observer);%km

[~, colat, lon] = cspice_recsph(state_rect_stereo);%rad
%state_source_sph = [r; colat; lon]; %rad

stereo_rect = cspice_sphrec(1 , colat, lon);
clear r colat lon

%%% mro
target_sc   = 'MRO';
[ state_rect_mro ] = cspice_spkpos( target_sc, et, frame, abcorr, observer);%km

[~, colat, lon] = cspice_recsph(state_rect_mro);%rad
%state_source_sph = [r; colat; lon]; %rad

mro_rect = cspice_sphrec(2 , colat, lon);
clear r colat lon



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f1 = figure;
%axes('position',[0 0 1 1]);

scatter3(0,0,0, 'yellow'); %sun
hold on
scatter3(mars_rect(1),mars_rect(2), mars_rect(3), 'red')
hold on
scatter3(mro_rect(1),mro_rect(2),mro_rect(3),'green')
hold on
scatter3(stereo_rect(1), stereo_rect(2), stereo_rect(3), 'blue')
title(date_MRO)
legend('Sun', 'Mars', 'MRO', 'Stereo-A','Location','southoutside','NumColumns',3);
xlim([-3 3]);
ylim([-3 3]);
zlim([-3 3]);
hold off


saveas(f1, ['plot2d_sun_mars_MRO_stereo_' date_MRO '.png']);


