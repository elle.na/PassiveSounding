% calculates the time window

function [TimeInterval] = calulate_time_window(Time, colatitude_angle,theta_star_deg, th_angle)

t = cell2mat(Time);
t(:,end-1:end)=[];
t = num2cell(t,2);

Time_dt = datetime(t,'InputFormat','yyyy MMM dd HH:mm:ss'); % 2005-08-01 00:00:00.000000 UTC

step = Time_dt(2)-Time_dt(1);

diff_angle = abs(colatitude_angle-theta_star_deg);
% [index_angle,~]=find(colatitude_angle<=th_angle);

[~,index_angle, ~]=find(diff_angle<th_angle);

TimeInterval = {};

i_TI=1; i_time = 1;


while i_time<=length(index_angle)

 if (i_time==length(index_angle)) || (index_angle(i_time)+1 ~=index_angle(i_time+1)) % time non consecutivi

    TimeInterval{i_TI,1} = Time_dt(i_time)-step/2;
    TimeInterval{i_TI,2} = Time_dt(i_time)+step/2;
    TimeInterval{i_TI,3} = colatitude_angle(index_angle(i_time));% average angle value
    TimeInterval{i_TI,4} = theta_star_deg(index_angle(i_time));

 else % time consecutivi

    TimeInterval{i_TI,1} = Time_dt(i_time);
    v_colatitude= colatitude_angle(index_angle(i_time));
    v_theta_star = theta_star_deg(index_angle(i_time));
    while (index_angle(i_time)+1 == index_angle(i_time+1)) && (i_time<=size(index_angle,1))
        i_time=i_time+1;
    end

    TimeInterval{i_TI,2} = Time_dt(i_time);
    colatitude_angle(index_angle(i_time));
    TimeInterval{i_TI,3} = mean( [ v_colatitude  colatitude_angle(index_angle(i_time)) ] );%
    TimeInterval{i_TI,4} = mean( [ v_theta_star  theta_star_deg(index_angle(i_time)) ] );%

 end
 i_TI=i_TI+1;
 i_time=i_time+1;

end


%save(fn_out, 'TimeInterval');
 
end