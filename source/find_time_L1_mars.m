function [delta_T] = find_time_L1_mars(time_UTC)

[et] = cspice_str2et( datestr(time_UTC ));

speed_light = 299792.458; % km/s

mass_earth = 5.972 * 10^24; %kg
mass_sun = 1.989 * 10^30; % kg

[pos_earth, ~] = cspice_spkpos( 'EARTH', et,   'IAU_SUN', ...
                                      'LT+S',  'SUN'           );
post_sun = cspice_spkpos( 'SUN', et,   'IAU_SUN', ...
                                      'LT+S',  'SUN'           );
pos_mars = cspice_spkpos( 'MARS', et,   'IAU_SUN', ...
                                      'LT+S',  'SUN'           ); 

d_sun_earth = cspice_vdist(pos_earth,post_sun);

d_L1 = d_sun_earth * nthroot((mass_earth/ (3* mass_sun)),3);

d_mars_sun = cspice_vdist(pos_mars,post_sun);

d_L1_mars = d_L1+(d_mars_sun - d_sun_earth);

% delta_mars_earth = (d_mars_sun - d_sun_earth)/speed_light;

delta_T = d_L1_mars/speed_light; %s

return % function



