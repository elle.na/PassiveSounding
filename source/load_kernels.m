% kernels
% load kernels

% to get the MRO kernels: wget -m -nH --cut-dirs=5 -nv https://naif.jpl.nasa.gov/pub/naif/pds/data/mro-m-spice-6-v1.0/mrosp_1000/
% Semenov, B.V., C.H. Acton, and M. Costa Sitja, MARS RECONNAISSANCE
% ORBITER SPICE KERNELS V1.0, MRO-M-SPICE-6-V1.0, NASA Planetary Data System, 2007. https://doi.org/10.17189/1520100

% generic kernels
% wget -m -nH --cut-dirs=5 -nv https://naif.jpl.nasa.gov/pub/naif/generic_kernels/


mars_folder_kernel = [kernel_path 'MARS2020\kernels\'];
generic_kernel= [kernel_path 'generic_kernels\'];

addpath(genpath(kernel_mro_folder));
addpath(genpath(mars_folder_kernel));

% set kernels
leapsecond_kernel_file = 'lsk\naif0012.tls';
fk_kernel_file = 'fk\mro_v16.tf';
% jupiter kernel
generic_kernel_jup = 'spk\satellites\jup365.bsp';
% mars kernel
mars_kernel_file ='spk\mar097.bsp';
% MRO kernel
%sc_kernel_file = ['spk\' fn_mro_kernel];
% sc_kernel_file = 'spk\mro_psp3.bsp';
% IAU_mars frame kernel
pck_mars_kernel = 'pck\pck00010.tpc';
% gravity kernel
gravity_kernel='pck\gm_de431.tpc';

% load kernels
cspice_furnsh([kernel_mro_folder leapsecond_kernel_file]);
cspice_furnsh([generic_kernel generic_kernel_jup]);
cspice_furnsh([generic_kernel gravity_kernel]);
cspice_furnsh([mars_folder_kernel pck_mars_kernel]);
cspice_furnsh([kernel_mro_folder fk_kernel_file]);
cspice_furnsh([kernel_mro_folder mars_kernel_file]);
%cspice_furnsh([kernel_mro_folder sc_kernel_file]);


