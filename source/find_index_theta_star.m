function [idx_plt, sc_altitude,spoint] =find_index_theta_star(state_sc_sph,observer, time_array, abcorr, target_sc, diff_zx_rotated, delta_theta_star)
    
    load('C:\Users\admin\Desktop\GIT\PassiveSounding\spec_theta(h).mat','theta_sc_star','hh')
    theta_sc_star = double(theta_sc_star);

    radii  = cspice_bodvrd( 'MARS', 'RADII', 3 ); %m

    fl_mars_radius_complex = 1; 
    if fl_mars_radius_complex == 0
        
        sc_altitude = (state_sc_sph(1,:)-radii(1))';
        
        spoint =[];
    
    else
        re = radii(1); % equatorial radius
        rp = radii(3); % polar radius
        f = ( re-rp)/re;

        method = { 'intercept', 'ellipsoid' };
        % 1 -- Intercept:  ellipsoid = target surface intercept
        % 2 -- near point

        i_method = 1;

        [spoint, sc_altitude] = cspice_subpt( method(i_method), observer, time_array, abcorr, target_sc );
    end

    % DONE: find a better way to compute the spacecraft altitude
    % radius wrt to the sc coordinate
    % try: surface intercept point 
    % (https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/cspice/subpnt_c.html)

    orbit_altitude = (hh/1000)'; % convert m in km

    sc_altitude_round = round(sc_altitude');

    temp = repmat(orbit_altitude,[1 length(sc_altitude_round)]);
    [minValue,closestIndex] = min(abs(temp-sc_altitude_round'));
    closestValue_Orbit = orbit_altitude(closestIndex); 
    clear temp

    %theta_star_rad = zeros(size(closestValue)); 
    theta_star_rad = theta_sc_star(1, closestIndex);

    theta_star_deg_th= rad2deg(theta_star_rad);
    
    diff = abs(theta_star_deg_th - diff_zx_rotated);
    
    [~,idx_plt] = find(diff <= delta_theta_star);
    
    %disp(length(idx_plt));
    
    
end