%% Calculate the state vectors

function [diff_zx_rotated_deg,time_UTC,state_sc_sph,time_array,state_sc_rect]=calculate_state_vectors(target_source,target_sc,frame,abcorr,observer, time_interval,timeWindow_mro, fl_video)

% parameters
%step = sf*400.;
% target_source   = 'JUPITER'; % JUPITER
% target_sc   = 'MRO';
% frame    = 'IAU_MARS';
% abcorr   = 'LT+S';
% observer = 'Mars';
% time_interval = 10; % seconds

%     epoch1    = '2006-09-12 00:00:00.000000 UTC';
%     et1 = cspice_str2et( epoch1 );
%     epoch2    = '2006-09-12 00:00:10.000000 UTC';
%     et2 = cspice_str2et( epoch2 );
%     time_interval = et2-et1; % second
%     
    perc = 1; % after the 
    format='C';
%     utcstr= cspice_et2utc( timeWindow_mro(1), format, perc );

time_array = timeWindow_mro(1)+1:time_interval:timeWindow_mro(2);
time_array_UTC =  cspice_et2utc( time_array, format, perc );
time_UTC = num2cell(time_array_UTC,2);

% MRO
[ state_sc_rect ] = cspice_spkpos( target_sc, time_array, frame, abcorr, observer);%km
% [ state_sc_rect ] = cspice_spkezr( target_sc, time_array, frame, abcorr, observer);%km
% state_sc Cartesian position vector(s) in KM

[r, colat, lon] = cspice_recsph(state_sc_rect); %rad
state_sc_sph = [r; colat; lon];%rad

state_sc_rec_r05 = cspice_sphrec(0.5*ones(size(colat)) , colat, lon);
clear r colat lon


% JUPITER
[ state_source_rec ] = cspice_spkpos( target_source, time_array, frame, abcorr, observer); %km
% [ state_source_rec ] = cspice_spkezr( target_source, time_array, frame, abcorr, observer);%km

[r, colat, lon] = cspice_recsph(state_source_rec);%rad
state_source_sph = [r; colat; lon]; %rad

state_source_rec_r1 = cspice_sphrec(1*ones(size(colat)) , colat, lon);
clear r colat lon

%% angular separation - NOT rotated planes

% angular separation xy -> Longitude
diff_xy = rad2deg(state_source_sph(3,:)' - state_sc_sph(3,:)');

figure; plot(diff_xy);
title(['Angular separation on the xy plane (Longitude) from ' time_UTC{1} ' to ' time_UTC{end}]);
xlabel(['Time with steps of ' num2str(time_interval) ' s']);
ylabel('Longitude in deg')

% angular separation zx -> colatitude
diff_zx = rad2deg(state_source_sph(2,:) - state_sc_sph(2,:));

figure; plot(diff_zx)
title(['Angular separation on the zx plane (colatitude) from ' time_UTC{1} ' to ' time_UTC{end}]);
xlabel(['Time with steps of ' num2str(time_interval) ' s']);
ylabel('Colatitude in deg')

% video
if fl_video ==1
    filename = 'planets.gif';
    scatter_plot_planets(filename,...
        state_sc_rec_r05(1,:),state_sc_rec_r05(2,:),state_sc_rec_r05(3,:),...
        time_UTC,...
        state_source_rec_r1(1,:),state_source_rec_r1(2,:),state_source_rec_r1(3,:)); 
end

%% angular separation - rotated planes
rotated_state_source_rec_r1 = zeros(size(state_source_rec));
rotated_state_sc_rec_r05 = zeros(size(state_sc_rect));

% rotate the coordinate system at each timestamp
for i_time = 1:1:size(state_sc_rect,2)
    
        rotated_state_source = [0;0;1];
        R = vrrotvec(state_source_rec_r1(:,i_time),rotated_state_source);
        matr = vrrotvec2mat(R);
        
        rotated_state_source_rec_r1(:,i_time) = round(matr*state_source_rec_r1(:,i_time));
        rotated_state_sc_rec_r05(:,i_time) = matr *state_sc_rec_r05(:,i_time);
               
end

% calculate the spherical coordinate
[r, colat, lon] = cspice_recsph(rotated_state_source_rec_r1);
rotated_state_source_sph_r1 = [r; colat; lon];
clear r colat lon
 
[r, colat, lon] = cspice_recsph(rotated_state_sc_rec_r05);
rotated_state_sc_sph_r05 =[r; colat; lon];
clear r colat lon

% angular separation is on zx -> colatitude
diff_zx_rotated_deg =rad2deg(rotated_state_sc_sph_r05(2,:));

figure; plot(diff_zx_rotated_deg);
title(['Angular separation on the rotated zx plane (colatitude) from ' time_UTC{1} ' to ' time_UTC{end}]);
xlabel(['Time with steps of ' num2str(time_interval) ' s']);
ylabel('Colatitude in deg')

if fl_video == 1
    filename = 'planets_roteted.gif';
    scatter_plot_planets(filename,...
        rotated_state_sc_rec_r05(1,:),rotated_state_sc_rec_r05(2,:),rotated_state_sc_rec_r05(3,:),...
        time_UTC,...
        rotated_state_source_rec_r1(1,:),rotated_state_source_rec_r1(2,:),rotated_state_source_rec_r1(3,:)); 
end


end