function [f1] = scatter_plot_planets(filename, x_J, y_J, z_J,date_MRO, x_MRO, y_MRO, z_MRO)

%filename='planets_rotated.gif';
f1 = figure;
%axes('position',[0 0 1 1]);
k=1;
scatter3(x_J(k),y_J(k),z_J(k), 'black');
hold on
scatter3(0,0,0, 'red')
hold on
scatter3(x_MRO(k),y_MRO(k),z_MRO(k),'green')
title(date_MRO{k})
legend('Jupiter', 'Mars', 'MRO','Location','southoutside','NumColumns',3);
xlim([-1 1]);
ylim([-1 1]);
zlim([-1 1]);
hold off
drawnow
frame = getframe(f1) ;
im = frame2im(frame);
[imind,cm] = rgb2ind(im,256);
imwrite(imind,cm,filename,'gif', 'Loopcount',inf);

%for k = 2:length(x_MRO) 
% for k = 2:10:length(x_MRO)
for k = 1:length(x_MRO)
    scatter3(x_J(k),y_J(k),z_J(k), 'black');
    hold on
    scatter3(0,0,0, 'red')
    hold on
    scatter3(x_MRO(k),y_MRO(k),z_MRO(k),'green')
    title(date_MRO{k})
    legend('Jupiter', 'Mars', 'MRO','Location','southoutside','NumColumns',3);
    xlim([-1 1]);
    ylim([-1 1]);
    zlim([-1 1]);
    hold off
     % pause 2/10 second: 
     pause(0.2)
     hold off
     drawnow
     frame = getframe(f1);
     im = frame2im(frame);
     [imind,cm] = rgb2ind(im,256);    

     imwrite(imind,cm,filename,'gif','WriteMode','append');
end

