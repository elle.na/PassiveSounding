% %% create a mat file with sharad acquisition properties
% % input from https://pds-geosciences.wustl.edu/missions/mro/sharad.htm
% 
% clear 
% close all
% 
% cd('C:\Users\admin\Desktop\GIT\PassiveSounding')
% link_sharad = 'https://pds-geosciences.wustl.edu/mro/mro-m-sharad-3-edr-v1/mrosh_0001/';
% 
% % start and ending time column
% start_time_column = 11;
% end_time_column = 12;
% link_column = 3;
% nb_file=4;
% 
% path_data = 'data_sharad';
% fn_data = 'cumindex_';


function [start_time, end_time, link_lbl] = extract_sharad_times(fn_data,path_data,nb_file,link_column, end_time_column,start_time_column)
start_time=[];
end_time =[];
link_lbl=[];
for i_file = 1:nb_file
    %% load tab data
    opts = detectImportOptions([path_data '\' fn_data num2str(i_file) '.csv'], 'Delimiter', ',','VariableNamingRule','preserve');
    table_sharad = readtable([path_data '\' fn_data num2str(i_file) '.csv'],opts);
    clear opts

    %% extract the data
    % YYYY-DDDThh:mm:ss.mmm
    start_time_temp = datetime(eval(['table_sharad.Var' num2str(start_time_column)]),'InputFormat','uuuu-DDD''T''HH:mm:ss.SSS'); % 2005-08-01 00:00:00.000000 UTC
    end_time_temp = datetime(eval(['table_sharad.Var' num2str(end_time_column)]),'InputFormat','uuuu-DDD''T''HH:mm:ss.SSS'); % 2005-08-01 00:00:00.000000 UTC
    
    link_lbl_capital_temp= eval(['table_sharad.Var' num2str(link_column)]);
    
    %link_lbl_capital = cell2mat(link_lbl_capital);

    start_time=[start_time;  start_time_temp];
    end_time=[end_time;  end_time_temp];
    
    link_lbl = [link_lbl; link_lbl_capital_temp];
end

%% da fare per ogni dato
% link_lbl_capital = cell2mat(link_lbl_capital);
% link_lbl_temp = [link_sharad lower(link_lbl_capital)];

end