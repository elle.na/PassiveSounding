% clc
% clear
% close all

function [nb_burst, solar_burst]= analyze_dynamic_specra_stereo(date,fl_plot)
% https://stereo-ssc.nascom.nasa.gov/data/ins_data/swaves/2019/swaves_average_20190127_a_hfr.dat
%RunMExceptions = [];

%date = '20220422'; %'20100826';%'20100826';%'20061027';
%fl_plot=1;

fl_high_freq=1;

% old link
%link_stereo = 'https://stereo-ssc.nascom.nasa.gov/data/ins_data/swaves/';

% new link (backup)
link_stereo = 'https://solar-radio.gsfc.nasa.gov/data/stereo/summary/';

fname = ['swaves_average_' date '_a.sav'];
%link_date = [date(1:4) '/swaves_average_' date '_a_hfr.dat'];
link_date = [date(1:4) '/swaves_average_' date '_a.sav'];

link = [link_stereo link_date];

% wget
try
    outfilename = websave(fname,link);

catch ME
   disp('Stereo data not available online');
   disp(ME.message);

   %RunMExceptions = [RunMExceptions; ME]; % store for later use
   
   nb_burst=0;
   solar_burst=[];
   return
end

%outfilename = ['swaves_average_' date '_a.sav'];

% read
% data = importfile_stereo(outfilename, [1, Inf]);
% spectra = table2array(data(3:end,3:end))';
% freq_khz = table2array(data(1,:))';

outargs=restore_idl(outfilename,'verbose');
spectra = outargs.SPECTRUM(50:end,:);
freq_khz = outargs.FREQUENCIES(50:end);

%placemarker_value = mean(spectra(:));
spectra(spectra<mean(spectra(:)))= mean(spectra(:));%min(abs(spectra(:)));

spectra(spectra<mean(spectra(:)))= mean(spectra(:));

spectra(spectra<mean(spectra(:)))= mean(spectra(:));

% db
spectra_db = pow2db(abs(spectra));


H_LP = fspecial('average',[2 1])';
spectra_LP = imfilter(spectra_db,H_LP);

spectra_db=spectra_LP(3:60,:);
y_freq=freq_khz(3:60)/1000;
y_freq_khz_all=freq_khz(3:end)/1000;

% statistics
mean_tot = mean(mean(spectra_db));
sdt_tot = mean(std(spectra_db));

% preliminary check for peaks
if sdt_tot<0.1
    fl_peaks=0;

    nb_burst=0;
    solar_burst=[];

    str_date = fname(1:end-2);
    if fl_plot
        figure;
        x_time = cumsum(ones(size(spectra_db,2),1))/60;
        imagesc(x_time,y_freq_khz_all,pow2db(abs(spectra)));  colormap('jet');
        grid on; grid minor
        %caxis([0.5 2])
    %     a=colorbar;
    %     a.Label.String = 'Normalized power (dB)';
        ylabel('Frequency (MHz)')
        xlabel('Time (hour)')
        %set(gca,'ytick',[])
        title(['Dynamic spectra ' str_date], 'Interpreter', 'none')
        fn_fig = ['Dynamic_spectra_' str_date '_candidate_peaks.fig'];
        savefig(fn_fig)
        saveas(gcf,['Dynamic_spectra_' str_date '_candidate_peaks.png'])
     end
    return
else 
    fl_peaks=1;
end

mean_spectra = mean(spectra_db);

x_time = cumsum(ones(size(mean_spectra)))/60; % ore

%y_freq(end)=y_freq(end-1)+100;

th_peak = sdt_tot;
i_burst_ok=0;

if th_peak==-Inf
    nb_burst=0;
    solar_burst=[];
else
    % disp(th_peak)

    if fl_plot==1
        str_date = fname(1:end-2);

        f1=figure;
        sb(1)=subplot(211);
        imagesc(x_time,y_freq_khz_all,pow2db(abs(spectra)));  colormap('jet');
        %caxis([0.5 2])
    %     a=colorbar;
    %     a.Label.String = 'Normalized power (dB)';
        ylabel('Frequency (MHz)')
        xlabel('Time (hour)')
        %set(gca,'ytick',[])
        title(['Dynamic spectra ' str_date], 'Interpreter', 'none')

        sb(2)=subplot(212);

%         plot(mean_spectra)
%         hold on
        findpeaks(mean_spectra,'MinPeakProminence',th_peak,'Annotate','extents')
        grid on; grid minor
        %findpeaks(mean_spectra,'Annotate','extents')        
        % plot(mean_spectra_norm)
        hold off
        ylabel('Normalized Power (dB)')
        xlabel('Time [min]')
        %xlim([1 length(mean_spectra)])
        %set(gca,'ytick',[])
        title('Average dynamic spectra')

        %linkaxes(sb,'x')

        fn_fig = ['Dynamic_spectra_' str_date '_candidate_peaks.fig'];
        
        savefig(fn_fig)
        
        saveas(gcf,['Dynamic_spectra_' str_date '_candidate_peaks.png'])

        close all
    end

    if fl_peaks
        [pks,locs,w,~] = findpeaks(mean_spectra,'MinPeakProminence',th_peak); %daspect([1 1 1]);
        
    else
        pks=[];
    end
    
    locs_id = [];
    if ~isempty(pks)
        %nb_burst=size(pks,2);

        for i_burst =1:size(pks,2)

            i_burst;
            % check burst
            height = pks(i_burst);
            width = w(i_burst)/6;
            loc = locs(i_burst);
            


            if height>width && height> mean(mean_spectra(:))+1.5% 1. crisp rule

                %2. width vs height
                a = 20;
                c = 0.5;
                membership_heigh_width = sigmf(abs(height/width),[a c]);
%                 membership_heigh_width = feval(@sigmf,abs(width/height),[a c]);
%                 membership_heigh_width = feval(@sigmf,abs(width/width),[a c]);

               % 3. width simmetric?
               i_burst;
               
               if locs(i_burst) - round(1.5*width*6) <=1
                   width_left = locs(i_burst);
                   area_left = mean_spectra(locs(i_burst));
               else
                   sgn_prev = mean_spectra(locs(i_burst) - round(1.5*width*6):loc);
                   [~,idx_min_prev] = min(abs(sgn_prev - (height/2)));
                   
                   if length(sgn_prev) - idx_min_prev <1
                       width_left=1;
                   else
                        width_left= length(sgn_prev) - idx_min_prev;
                   end
                   
%                    width_left
%                    size(sgn_prev)

                   area_left = trapz(sgn_prev(width_left:end));
               end
               %init_burst = loc - round(1.5* width) + idx_min_prev -1;

               if locs(i_burst) + round(2* width*6)> length(mean_spectra)
                   width_right = length(mean_spectra) - locs(i_burst);
                   area_right = trapz(mean_spectra(locs(i_burst):end));
               else             
                   sgn_after = mean_spectra(locs(i_burst) : locs(i_burst) + round(2* width*6));
                   [~,idx_min_after] = min(abs(sgn_after - (height/2)));
                   width_right = idx_min_after;
                   area_right = trapz(sgn_after(1:width_right));
               end

%                figure; plot( mean_spectra(loc - round( width):loc))             
%                figure; plot( mean_spectra(loc: locs(i_burst) + round(width)))
%                figure; plot( mean_spectra(init_burst: end_burst))

               a1 = 5;
               c1 = 1;
               
               %membership_width_asym = sigmf(abs(1),[a1 c1])
               
               membership_width_asym = sigmf(abs(area_right/area_left),[a1 c1]);

%                 disp(['heigh width membership ' num2str(membership_heigh_width)])
%                 disp(['asymmetric memership ' num2str(membership_width_asym)]);
%                
               memebership_tot = membership_heigh_width * membership_width_asym;

%                 disp(['tot memership ' num2str(memebership_tot)]);

               if memebership_tot > 0.5
                   
                   
                   if fl_high_freq
                   
                       % To compute the closest value in a vector “N” for each element of “V”
                       [~,closestIndex_13]=find_closest_arrayIdx_to_array(y_freq,13000);
                       spectra_HF = spectra(closestIndex_13:end-10,:);
                       %x_time_HF = x_time(closestIndex_13:end-10);
                       
                       power_HF = mean(spectra_HF(:,locs(i_burst)));
                       
                       
                       %,y_freq,pow2db(abs(spectra))
                       if power_HF > mean(mean_spectra(:))+1.5
                            fl_burst_high=1;
                       else
                           fl_burst_high=0;
                       end
                       
                       if fl_burst_high
                           i_burst_ok = i_burst_ok+1;

                           % found burst
                            minu=locs(i_burst);
                            solar_burst_time = minutes(minu);
                            solar_burst_time.Format = 'hh:mm:ss';
                            solar_burst_time = datetime([date '000000'],'InputFormat','yyyyMMddhhmmss') +solar_burst_time;
                            % t = datetime(DateStrings,'InputFormat','yyyy-MM-dd')

                            solar_burst_width = minutes(ceil(w(i_burst)));
                            %solar_burst_width;

                            solar_burst(i_burst_ok).time = solar_burst_time;
                            solar_burst(i_burst_ok).width = solar_burst_width;
                            solar_burst(i_burst_ok).pks = pks(i_burst);
                            nb_burst = i_burst_ok;
                            locs_id = [locs_id i_burst];
                       end
                   else
                       i_burst_ok = i_burst_ok+1;

                       % found burst
                        minu=locs(i_burst);
                        solar_burst_time = minutes(minu);
                        solar_burst_time.Format = 'hh:mm:ss';
                        solar_burst_time = datetime([date '000000'],'InputFormat','yyyyMMddhhmmss') +solar_burst_time;
                        % t = datetime(DateStrings,'InputFormat','yyyy-MM-dd')

                        solar_burst_width = minutes(ceil(width));
                        %solar_burst_width;

                        solar_burst(i_burst_ok).time = solar_burst_time;
                        solar_burst(i_burst_ok).width = solar_burst_width;
                        solar_burst(i_burst_ok).pks = pks(i_burst);
                        nb_burst = i_burst_ok;
                        locs_id = [locs_id i_burst];
                   end
                    
               else
                   %locs_backup()=[];
               end

            end


        end

    end



end

if i_burst_ok==0

    nb_burst=0;
    solar_burst=[];
end



disp(['solar burst after fuzzy: ' num2str(nb_burst)])
    

if nb_burst~=0 && fl_plot==1
    
    % create ticks
    mean_spectra=mean(spectra_db);
    
    % plot
    str_date = fname(1:end-2);

    f2 = figure;
    sb(1)=subplot(211);
    imagesc(x_time,y_freq_khz_all,pow2db(abs(spectra)));  colormap('jet');
    %caxis([0.5 2])
%     a=colorbar;
%     a.Label.String = 'Normalized power (dB)';
    ylabel('Frequency (MHz)')
    xlabel('Time (hour)')
    %set(gca,'ytick',[])
    title(['Dynamic spectra ' str_date], 'Interpreter', 'none')

    sb(2)=subplot(212);

%         plot(mean_spectra)
%         hold on
    plot(mean_spectra)
    %findpeaks(mean_spectra,'Annotate','extents')        
    % plot(mean_spectra_norm)
    hold on
    scatter(locs(locs_id), pks(locs_id), '*', 'r' )
    hold off
    ylabel('Normalized Power (dB)')
    xlabel('Time [min]')
    xlim([1 length(mean_spectra)])
    %set(gca,'ytick',[])
    title('Average dynamic spectra')

    %linkaxes(sb,'x')

    fn_fig = ['Dynamic_spectra_' str_date '_ok_peaks.fig'];
    savefig(f2,fn_fig)
    
    saveas(f2, ['Dynamic_spectra_' str_date '_ok_peaks.png'])

end
    



