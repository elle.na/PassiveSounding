function [diff_colat,diff_long] = find_angle_sc_mars(time_UTC, sc )

[et] = cspice_str2et( datestr(time_UTC ));

frame    = 'IAU_SUN';
abcorr   = 'LT+S';
observer = 'SUN';

%% state vector stereo 
% rect coordinate
target_sc   = 'STEREO Ahead';
[ state_rect_stereo ] = cspice_spkpos( target_sc, et, frame, abcorr, observer);%km

% convert in spherical
[r, colat, lon] = cspice_recsph(state_rect_stereo); %rad
state_sc_sph_stereo = [r; colat; lon];

%% state vector mars
target_sc   = 'MARS';
[ state_rect_mars ] = cspice_spkpos( target_sc, et, frame, abcorr, observer);%km

% convert in spherical
[r, colat, lon] = cspice_recsph(state_rect_mars); %rad
state_sc_sph_mars = [r; colat; lon];


%% calculate angle difference

% longitude diff
diff_long = rad2deg(state_sc_sph_stereo(3,:)' - state_sc_sph_mars(3,:)');

% colatitude diff
diff_colat = rad2deg(state_sc_sph_stereo(2,:) - state_sc_sph_mars(2,:));
