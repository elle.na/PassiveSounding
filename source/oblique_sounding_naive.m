c  = 299792458;


%% SHARAD
% nominal
hnlow  = 255e3;
hnhigh = 320e3;

%extended
helow  = 230e3;
hehigh = 407e3;

2*hnlow/c*1e6   % in microsecs
2*hnhigh/c*1e6  % in microsecs
2*helow/c*1e6   % in microsecs
2*hehigh/c*1e6  % in microsecs


% PRF
prfnominal = 700.28;
prfloworb  = 775.19;
prfhigho   = 670.22;

1/prfnominal*1e6    % in microsecs
1/prfloworb*1e6     % in microsecs
1/prfhigho*1e6      % in microsecs

% let's compute oblique
tr  = 1428e-6;
trr = 1563e-6;
theta = acos((c*tr)/hnhigh-1)*180/pi

hparam = 255e3:100:320e3;
thetaparam = acos((c*tr)./hparam-1)*180/pi

figure
plot(hparam/1e3, thetaparam, 'Linewidth', 2)
title('Naïve oblique sounding')
xlabel('SHARAD altitude [km]')
ylabel('Angle \theta* needed for perfect overlap [deg]')
grid on


%---
% tr(theta) plot
theta_p = 0:0.1:90;
tparam  = (1+cosd(theta_p))*hnlow/c
figure
plot(theta_p, tparam*1e6, 'Linewidth', 2)
xlabel('\theta_{sc} [deg]')
ylabel('t_r [\mu s]')
title('SHARAD, h=255km')
grid on


%% MARSIS
prf = 127;
pri = 1/prf*1e6

orbitlow = 250e3;
orbithigh = 900e3;

2*orbitlow/c*1e6
2*orbithigh/c*1e6