function [timeWindow_mro_UTC,num_rdr, SharadLinkLBL,TimeInterval_colatitude,n_rdr_tot]= extract_lbl_list(mro_kernel_file, delta_theta_star,n_rdr_tot)

close all
% dir_path_csv = 'C:\Users\admin\Desktop\GIT\PassiveSounding\coordinate\';

fl_video = 0;
fl_plot = 1;

% 2 direct configuration | 1 rough/fix analysis analysis | 0 fine/chris based analysis
fl_rough_analysis = 2;
if fl_rough_analysis ==2
    angle_deg =0; % 
end
%%%% for direct configuration change the parameter below
delta_theta_star = 90; % deg

% 0 look for sharad data, 1 there are no sharad data
fl_no_sharad=0;

% true anomalies flag
fl_true_anomalies_analysis=0;
th_deg_TA=90; %deg

% 0 one solar burst in the day it's ok
% 1 looking for the exact timing of solarburst and sharad
fl_solar_burst_fine = 1;

% solar probe: 1 =wind, 2 = stereo
fl_solar_probe=2;
if fl_solar_probe==2
    th_colat_burst = 90; %deg
    th_long_burst = 90; %deg
end

%% set the time window of the sc from the kernels

fl_timeWindow_manual = 0;
if fl_timeWindow_manual ==1
    timeWindow_mro_UTC(1,:) = '2006 DEC 12 12:00:00';
    timeWindow_mro_UTC(2,:) = '2006 DEC 31 12:00:00';
    
    timeWindow_mro(1,:) = cspice_str2et( timeWindow_mro_UTC(1,:) );
    timeWindow_mro(2,:) = cspice_str2et( timeWindow_mro_UTC(2,:) );
else
    
    timeWindow_mro = cspice_spkcov( mro_kernel_file, cspice_bodn2c('MRO'), 1e6 );
    format = 'C';
    prec=0;
    timeWindow_mro_UTC = cspice_et2utc( timeWindow_mro', format, prec );
    % size(timeWindow_mro) = [2,1]
    
%     disp(timeWindow_mro_UTC(1,:))
%     disp(timeWindow_mro_UTC(2,:))
end

%% calculate the state vectors
target_source   = 'SUN'; % JUPITER
target_sc   = 'MRO';
frame    = 'IAU_MARS';
abcorr   = 'LT+S';
observer = 'Mars';
time_interval = 10; % seconds

if strcmp(target_source, 'SUN')
    fl_solar_burst = 1;
    
    if fl_solar_probe == 2 % stereo
        stereo_kernel_tm = 'E:\Kernel\STEREO\stereo_a_kernels.tm';
        cspice_furnsh(stereo_kernel_tm);
        
        timeWindow_stereo_init = cspice_spkcov( 'E:\Kernel\STEREO\ssw\stereo\gen\data\spice\depm\ahead\ahead_2006_350_01.depm.bsp', -234, 1e6 );
        timeWindow_stereo_end = cspice_spkcov( 'E:\Kernel\STEREO\ssw\stereo\gen\data\spice\depm\ahead\ahead_2022_174_01.depm.bsp', -234, 1e6 );
                
        timeWindow_stereo(1)=timeWindow_stereo_init(1);
        timeWindow_stereo(2)=timeWindow_stereo_end(2);
        
        format = 'C';
        prec=0;
        
%         timeWindow_stereo_UTC_init = cspice_et2utc( timeWindow_stereo_init', format, prec );
%         timeWindow_stereo_UTC_end = cspice_et2utc( timeWindow_stereo_end', format, prec );
%         
        timeWindow_stereo_UTC = cspice_et2utc( timeWindow_stereo, format, prec );
        
        if (timeWindow_mro(2) < timeWindow_stereo(1)) 
            fl_run_analysis =0;
        elseif (timeWindow_mro(1) >= timeWindow_stereo(1)) || (timeWindow_mro(2) <= timeWindow_stereo(2))
            fl_run_analysis =1;
        elseif (timeWindow_mro(1) >= timeWindow_stereo(2))
            fl_run_analysis =0;
        elseif (timeWindow_mro(1) < timeWindow_stereo(1)) && (timeWindow_mro(2) <= timeWindow_stereo(2))
            fl_run_analysis =1;
            timeWindow_mro(1) = timeWindow_stereo(1);
            timeWindow_mro_UTC(1) = timeWindow_stereo_UTC(1);
        elseif (timeWindow_mro(1) >= timeWindow_stereo(1))&& (timeWindow_mro(2) > timeWindow_stereo(2))
            fl_run_analysis =1;
            timeWindow_mro(2) = timeWindow_stereo(2);
            timeWindow_mro_UTC(2) = timeWindow_stereo_UTC(2);           
            
        end
        solar_burst_table=[];
    else
        fl_run_analysis =1;
    end
    
else
    fl_solar_burst=0;
    fl_run_analysis =1;
end

if fl_run_analysis

    % state vectors
    % [diff_zx_rotated_deg,time_UTC,state_sc_sph,time_array,state_sc_rect]= calculate_state_vectors(target_source,target_sc,frame,abcorr,observer, time_interval,timeWindow_mro,fl_video);
    [diff_zx_rotated_deg,time_UTC,state_sc_sph,time_array,~]= calculate_state_vectors(target_source,target_sc,frame,abcorr,observer, time_interval,timeWindow_mro,fl_video);

    close all



    %% Load the data for one day
    %load('data_one day.mat','theta_sc_star','diff_zx_rotated', 'time_UTC','time_interval')
    %diff_zx_rotated_deg = diff_zx_rotated;

    %% MRO time intervals
    % find indeces with a proper colatitude angle --> idx_plot
    
    % rough analysis
    if fl_rough_analysis==1

        [idx_plt,val_idx_plt] = find_index_theta_star_rough(diff_zx_rotated_deg, time_UTC, time_interval,fl_plot,delta_theta_star);

    elseif fl_rough_analysis ==2 % angle 

        [idx_plt,val_idx_plt] = find_index_direct_configuration(diff_zx_rotated_deg, time_UTC, time_interval,fl_plot,angle_deg, delta_theta_star);

    elseif fl_rough_analysis == 0 % finer analysis

        %delta_theta_star = 6; % deg
        [idx_plt,sc_altitude] = find_index_theta_star(state_sc_sph,observer, time_array, abcorr, target_sc,diff_zx_rotated_deg,delta_theta_star);
        val_idx_plt = diff_zx_rotated_deg(idx_plt);

        if fl_plot==8
            figure; 

            plot(diff_zx_rotated_deg);
            hold on
            plot(idx_plt, val_idx_plt,'r.');

            hold off

            title(['Angular separation on the rotated zx plane (colatitude) from ' time_UTC{1} ' to ' time_UTC{end}]);
            xlabel(['Time with steps of ' num2str(time_interval) ' s']);
            ylabel('Colatitude in deg')


            figure; 
            plot(sc_altitude);

            title(['Spacecraft Altitude from ' time_UTC{1} ' to ' time_UTC{end}]);
            xlabel(['Time with steps of ' num2str(time_interval) ' s']);
            ylabel('Altitude in km')


            figure; 
            id_in = 22000;
            id_out = 28300;
            plot(diff_zx_rotated_deg(id_in:id_out));
    %         hold on
    %         plot(idx_plt(id_in:id_out), val_idx_plt(id_in:id_out),'g.');
    % 
    %         hold off

            title(['Angular separation on the rotated zx plane (colatitude) from ' time_UTC{id_in} ' to ' time_UTC{id_out}]);
            xlabel(['Time with steps of ' num2str(time_interval) ' s']);
            ylabel('Colatitude in deg')

            figure; 
            plot(sc_altitude(id_in:id_out));

            title(['Spacecraft Altitude from ' time_UTC{id_in} ' to ' time_UTC{id_out}]);
            xlabel(['Time with steps of ' num2str(time_interval) ' s']);
            ylabel('Altitude in km')


            figure;

            yyaxis left
            plot(diff_zx_rotated_deg(id_in:id_out));        
            ylabel('Rotated colatitude in deg')

            yyaxis right
            plot(sc_altitude(id_in:id_out));
            ylabel('Altitude in km')

            title(['Spacecraft Altitude from ' time_UTC{id_in} ' to ' time_UTC{id_out}]);
            xlabel(['Time with steps of ' num2str(time_interval) ' s']);


             figure;

            yyaxis left
            plot(diff_zx_rotated_deg);        
            ylabel('Rotated colatitude in deg')

            yyaxis right
            plot(sc_altitude);
            ylabel('Altitude in km')

            title(['Spacecraft Altitude from ' time_UTC{1} ' to ' time_UTC{end}]);
            xlabel(['Time with steps of ' num2str(time_interval) ' s']);


        end


    end


    close all

    % find time intervals with a proper colatitude angle
    % uncomment for MRO right configuration time windows
    %[TimeInterval_colatitude_MRO, start_time_mro, end_time_mro] = find_time_interval(idx_plt,diff_zx_rotated_deg,time_UTC);

    %% SHARAD active time intervals
    % find time intervals sharad is on

    % link_sharad = 'https://pds-geosciences.wustl.edu/mro/mro-m-sharad-3-edr-v1/mrosh_0001/';

    % start and ending time column
    start_time_column = 11;
    end_time_column = 12;
    link_column = 3;
    nb_file=1;

    path_data = 'C:\Users\admin\Desktop\GIT\PassiveSounding\data_sharad';
    fn_data = 'cumindex_';

    addpath(genpath(path_data));

    [start_time_sharad, end_time_sharad, link_lbl] = extract_sharad_times(fn_data,path_data,nb_file,link_column, end_time_column,start_time_column);


    Time_dt = datetime(time_UTC,'InputFormat','yyyy MMM dd HH:mm:ss.S'); % 2005-08-01 00:00:00.000000 UTC

    %%%%% Va cambiato qua
    if start_time_sharad(end) >= Time_dt(1) 

        [link_lbl_analysis,idx_indice_link,idx_plt_sharad,diff_zx_rotated_deg_idx_plt_sharad,start_time_sharad_analysis, end_time_sharad_analysis] =...
            find_index_colatitude_sharad(diff_zx_rotated_deg, time_UTC, time_interval,start_time_sharad, end_time_sharad,link_lbl);

        % diff_zx_rotated_deg_idx_plt_sharad = diff_zx_rotated_deg(idx_plt_sharad);

        % % % plot_agle_MRO_SHARAD(diff_zx_rotated_deg,idx_plt,val_idx_plt,fl_plot,time_UTC,start_time_sharad, end_time_sharad);


        %% find common indeces bwt sharad and mro

        [idx_time_common, i_sharad, ~] = intersect(idx_plt_sharad, idx_plt,'legacy');

        %[idx_time_common, i_sharad, i_mro] = intersect(idx_plt_sharad, idx_plt,'legacy');
        % idx_time_common = idx_plt_sharad(i_sharad);
        % idx_time_common = idx_plt(i_mro);

        ColatitudeCommon = diff_zx_rotated_deg(idx_time_common);
        %idx_link_sharad_mro = idx_indice_link(idx_time_common); 

        idx_link_sharad_mro = idx_indice_link(i_sharad); 

        if fl_plot==1
            figure;

            % time vs rotated colatitude angle
            plot(diff_zx_rotated_deg);
            hold on

            % val_idx_plt = diff_zx_rotated_deg(idx_plt)
            plot(idx_plt, val_idx_plt,'r.');

            hold on

            % diff_zx_rotated_deg_idx_plt_sharad = diff_zx_rotated_deg(idx_plt_sharad);
            plot(idx_plt_sharad, diff_zx_rotated_deg_idx_plt_sharad,'y.');

            hold on

            % ColatitudeCommon = diff_zx_rotated_deg(idx_common);
            plot(idx_time_common, ColatitudeCommon,'g.');

            hold off

            title(['Angular separation on the rotated zx plane (colatitude) from ' time_UTC{1} ' to ' time_UTC{end}]);
            xlabel(['Time with steps of ' num2str(time_interval) ' s']);
            ylabel('Colatitude in deg') 

            legend('Rotated Colatitude','MRO in the right configuration', 'SHARAD acquisition', 'Common Indeces')
            legend('Location','southoutside')

            savefig('AngularSeparation_Sharad_MRO.fig')
        end

        % Time interval= start time | end time | colatitude rotated | lenght of the
        % time window
        % uncomment for the time window having MRO in the right configuration and SHARAD on
        [TimeInterval_colatitude_MRO_SHARAD, start_time_MRO_SHARAD, end_time_MRO_SHARAD] = find_time_interval(idx_time_common, diff_zx_rotated_deg,time_UTC);

        [TimeInterval_Link, ~, ~] = find_time_interval_link(idx_time_common, i_sharad,idx_indice_link,time_UTC, cell2mat(link_lbl_analysis));

        if fl_true_anomalies_analysis==0 && fl_solar_burst==0
            TimeInterval_colatitude = TimeInterval_colatitude_MRO_SHARAD;
            start_time = start_time_MRO_SHARAD;
            end_time = end_time_MRO_SHARAD;
        end


        %% True anomalies
        if fl_true_anomalies_analysis==1
            % true anomalies
            true_anomaly_deg = calculate_true_anomaly_angle_from_SV(target_sc, time_array, frame, abcorr, observer);
            idx_true_anomalies_th = find(true_anomaly_deg<th_deg_TA);

            idx_time_common_temp=idx_time_common;
            clear idx_time_common
            [idx_time_common, ~, ~] = intersect(idx_true_anomalies_th, idx_time_common_temp,'legacy');

            ColatitudeCommon = diff_zx_rotated_deg(idx_time_common);
            Colatitude_TA = diff_zx_rotated_deg(idx_true_anomalies_th);

            % [idx_time_common, i_ta, i_common] = intersect(idx_true_anomalies_th, idx_time_common_temp,'legacy');
            % idx_time_common = idx_true_anomalies_th(i_ta);
            % idx_time_common = idx_time_common_temp(i_common);

            [TimeInterval_TA, start_time_TA, end_time_TA] = find_time_interval(idx_time_common,...
            true_anomaly_deg,time_UTC);

            if fl_plot==1
                figure;

                % time vs rotated colatitude angle
                plot(diff_zx_rotated_deg);
                hold on


                %Colatitude_TA = diff_zx_rotated_deg(idx_true_anomalies_th);
                plot(idx_true_anomalies_th, Colatitude_TA,'b.');


                hold on
                % val_idx_plt = diff_zx_rotated_deg(idx_plt)
                plot(idx_plt, val_idx_plt,'r.');

                hold on

                % diff_zx_rotated_deg_idx_plt_sharad = diff_zx_rotated_deg(idx_plt_sharad);
                plot(idx_plt_sharad, diff_zx_rotated_deg_idx_plt_sharad,'y.');

                hold on

                % ColatitudeCommon = diff_zx_rotated_deg(idx_common_TA);
                plot(idx_time_common, ColatitudeCommon,'g.');


                hold off

                title(['Angular separation on the rotated zx plane (colatitude) from ' time_UTC{1} ' to ' time_UTC{end}]);
                xlabel(['Time with steps of ' num2str(time_interval) ' s']);
                ylabel('Colatitude in deg') 

                legend('Rotated Colatitude','True Anomaly angle < 90 deg', 'MRO in the right configuration', 'SHARAD acquisition', 'Common Indeces')
                legend('Location','southoutside')

                savefig('AngularSeparation_Sharad_MRO_TA.fig')
            end

            % Time interval= start time | end time | colatitude rotated | lenght of the
            % time window
            % uncomment
            [TimeInterval_colatitude_MRO_SHARAD_TA, start_time_MRO_SHARAD_TA, end_time_MRO_SHARAD_TA] = find_time_interval(idx_time_common, diff_zx_rotated_deg,time_UTC);
            [~, i_sharad, ~] = intersect(idx_plt_sharad, idx_time_common,'legacy');
            idx_link_sharad_mro = idx_indice_link(i_sharad); 
            [TimeInterval_Link, ~, ~] = find_time_interval_link(idx_time_common, i_sharad,idx_indice_link,time_UTC, cell2mat(link_lbl_analysis));

            if fl_solar_burst==0
                TimeInterval_colatitude = TimeInterval_colatitude_MRO_SHARAD_TA;
                start_time = start_time_MRO_SHARAD_TA;
                end_time = end_time_MRO_SHARAD_TA;
            end

        end

        %% solar burst
        % fl_solar_burst=0;

        if fl_solar_burst==1

            if fl_true_anomalies_analysis ==1
                nb_tw = size(TimeInterval_colatitude_MRO_SHARAD_TA,1);     
                TimeInterval_colatitude_MRO_SHARAD_TA_SB=TimeInterval_colatitude_MRO_SHARAD_TA;
                start_time_MRO_SHARAD_TA_SB = start_time_MRO_SHARAD_TA;
                end_time_MRO_SHARAD_TA_SB = end_time_MRO_SHARAD_TA;
            else
                nb_tw = size(TimeInterval_colatitude_MRO_SHARAD,1);
                TimeInterval_colatitude_MRO_SHARAD_SB = TimeInterval_colatitude_MRO_SHARAD;
                start_time_MRO_SHARAD_SB = start_time_MRO_SHARAD;
                end_time_MRO_SHARAD_SB = end_time_MRO_SHARAD;
            end

            % for each sharad candidate rdr
            % identify date_solar_burst for that day
            % if any burst is ia bit before that the candidate -> ok, otherwise no

%             fl_NO_Tw=[]; % tw to remove from nb_tw because there are no solar burst
            fl_SI_Tw=[];
            fl_NO_Tw=[];
            for i_tw=1:nb_tw
                if fl_true_anomalies_analysis ==1
                    date_tocheck = start_time_MRO_SHARAD_TA(i_tw);
                    duration_tocheck = end_time_MRO_SHARAD_TA(i_tw) - start_time_MRO_SHARAD_TA(i_tw);          
                else
                    date_tocheck = start_time_MRO_SHARAD_SB(i_tw);
                    duration_tocheck = end_time_MRO_SHARAD_SB(i_tw) - start_time_MRO_SHARAD_SB(i_tw);
                end

                formatOut = 'yyyymmdd';
                date_solar_burst=datestr(date_tocheck,formatOut);

                if fl_solar_probe ==1

                    % detect the burst
                    [nb_burst, solar_burst]= analyze_dynamic_specra_wind(date_solar_burst,fl_plot);


                elseif fl_solar_probe ==2
                    % burst detection
                    if isfile(['solar_burst_' (date_solar_burst) '.mat']) % file solarburst_data
                        load(['solar_burst_' (date_solar_burst) '.mat'], 'solar_burst','nb_burst');
                        
                    else %calculate e salve
                        [nb_burst, solar_burst]= analyze_dynamic_specra_stereo(date_solar_burst,fl_plot);
                        save(['solar_burst_' (date_solar_burst) '.mat'], 'solar_burst','nb_burst');
                    end
                end
                disp([ 'burst numbers considered in the time compensation analysis: ' num2str(nb_burst)]);


             % compensate the time
                if nb_burst == 0 % there are no burst
                    fl_NO_Tw=[fl_NO_Tw i_tw];   
                else
                    if fl_solar_burst_fine == 1
                        fl_bs_ok = 0;
                        for i_burst =1:nb_burst
% %                             i_burst
                            % uncomment if needed
                            % start_burst = solar_burst(i_burst).time - (solar_burst(i_burst).width)/2;
                            % end_burst =solar_burst(i_burst).time + (solar_burst(i_burst).width)/2;


                            mid_tocheck = date_tocheck + duration_tocheck/2;

                            if fl_solar_probe ==1 % wind

                                % travelling time L1 to mars
                                [delta_T] = find_time_L1_mars(mid_tocheck);
                                % in seconds

                            else % stereo

                                [delta_T] = find_time_stereo_mars(mid_tocheck);
                                % in seconds
                            end
                            
%                             if strcmp('20100304', date_solar_burst)
%                                disp('pb')
%                                disp(datestr(mid_tocheck))                                
%                             end
                              
                            time_rdr = [date_tocheck; date_tocheck + duration_tocheck];
                            time_burst = [solar_burst(i_burst).time + seconds(delta_T)- solar_burst(i_burst).width;  solar_burst(i_burst).time + seconds(delta_T)+ solar_burst(i_burst).width];
                            
             
                                                       
                            % checking if the time is ok
                            % if (mid_tocheck - (solar_burst(i_burst).time + seconds(delta_T)))<= 2*max(duration_tocheck, solar_burst(i_burst).width)
                            % if (time_burst(1)<time_rdr(2)  && time_burst(1)>time_rdr(1)) || (time_burst(2)<time_rdr(2)  && time_burst(2)>time_rdr(1))
                            if ~(time_burst(2)<time_rdr(1) || time_burst(1)>time_rdr(2))
                            % checking the position of the probe respect to
                                % mars and the sun (only is stereo!)
                                if fl_solar_probe ==2
                                    
                                    [diff_colat,diff_long] = find_angle_sc_mars(mid_tocheck, 'STEREO Ahead' );
                                    % for now works only with stereo A and B -> needed spacecraft kernels
                                    
                                    if abs(diff_colat)<th_colat_burst && abs(diff_long)<th_long_burst 
                                        % check if they (MRO/Mars and stereo) are in the same half sphere
                                        
                                        rdr_id= TimeInterval_Link{i_tw,5};
                                        
                                        if fl_true_anomalies_analysis ==1
                                            mean_rotated_colat = TimeInterval_colatitude_MRO_SHARAD_TA_SB(i_tw,3)    ;
                                        else
                                            mean_rotated_colat = TimeInterval_colatitude_MRO_SHARAD_SB(i_tw, 3)      ;
                                        end
                                        
                                        if isempty(solar_burst_table)
                                            solar_burst_table.rdr_id=rdr_id;
                                            solar_burst_table.peak_time = solar_burst(i_burst).time;
                                            solar_burst_table.burst_duration_3db = solar_burst(i_burst).width;
                                            solar_burst_table.peak_power = solar_burst(i_burst).pks;
                                            solar_burst_table.mean_mars2sc_colat= diff_colat;
                                            solar_burst_table.mean_mars2sc_long = diff_long ;
                                            solar_burst_table.rotatedcolatMRO = mean_rotated_colat;
                                        else
                                            bb=size(solar_burst_table,2)+1;
                                            solar_burst_table(bb).rdr_id=rdr_id;
                                            solar_burst_table(bb).peak_time = solar_burst(i_burst).time;
                                            solar_burst_table(bb).burst_duration_3db = solar_burst(i_burst).width;
                                            solar_burst_table(bb).peak_power = solar_burst(i_burst).pks;
                                            solar_burst_table(bb).mean_mars2sc_colat= diff_colat;
                                            solar_burst_table(bb).mean_mars2sc_long = diff_long ;
                                            solar_burst_table(bb).rotatedcolatMRO = mean_rotated_colat;
                                        end
                                       
                                        
                                        %%%% plot the scatter plot
                                         % to check
                                        
                                        plot2d_planets_MRO_STEREO( mid_tocheck );
                                        
                                        fl_bs_ok=1;
                                        fl_SI_Tw = [fl_SI_Tw i_tw];
                                    else % check if they are in the same  half sphere
                                        % NOT in the same half sphere
                                        fl_bs_ok=0;
                                        %fl_NO_Tw = [fl_NO_Tw i_tw];
                                    end
                                    
                                else % wind
                                    fl_bs_ok=1;
                                    fl_SI_Tw = [fl_SI_Tw i_tw];
                                end % stereo or wind 
                                
                            else % not in the right time
                                fl_bs_ok=0;
%                                 fl_NO_Tw = [fl_NO_Tw i_tw];
                            end % end of the check if the time of the window and the burst are ok
                            
                        end % end time compensation + check for each burst
                        
%                         if fl_bs_ok == 0 % the burst is not ok
%                             fl_NO_Tw = [fl_NO_Tw i_tw];
%                         end
                        
                    end % if solar burst anlaayis is fine or not (not = one burst per day is ok, yes= check for the exact time + compensation)

                end % if no burst are detected for this time window


            end % end for cycle that check each entry in Time interval

            if fl_true_anomalies_analysis ==1

                TimeInterval_colatitude_MRO_SHARAD_TA_SB = TimeInterval_colatitude_MRO_SHARAD_TA_SB(fl_SI_Tw,:);
                start_time_MRO_SHARAD_TA_SB=start_time_MRO_SHARAD_TA_SB(fl_SI_Tw,:);
                end_time_MRO_SHARAD_TA_SB=end_time_MRO_SHARAD_TA_SB(fl_SI_Tw,:);

                TimeInterval_colatitude = TimeInterval_colatitude_MRO_SHARAD_TA_SB;
                start_time = start_time_MRO_SHARAD_TA_SB;
                end_time = end_time_MRO_SHARAD_TA_SB;

            else
                TimeInterval_colatitude_MRO_SHARAD_SB = TimeInterval_colatitude_MRO_SHARAD_SB(fl_SI_Tw,:);
                start_time_MRO_SHARAD_SB=start_time_MRO_SHARAD_SB(fl_SI_Tw,:);
                end_time_MRO_SHARAD_SB=end_time_MRO_SHARAD_SB(fl_SI_Tw,:);

                TimeInterval_colatitude = TimeInterval_colatitude_MRO_SHARAD_SB;
                start_time = start_time_MRO_SHARAD_SB;
                end_time = end_time_MRO_SHARAD_SB;
            end

            TimeInterval_Link=TimeInterval_Link(fl_SI_Tw,:);
            if ~isempty(TimeInterval_Link)
                [idx_link_sharad_mro] = recreate_idx_all(TimeInterval_Link);
            else
                % problema e' qua
                idx_link_sharad_mro=[];
            end     

        end % if solar burst

        close all
        %% TO DO
        % update TimeInterval_Link for the solar burst
        % take all the TimeInterval{i_TI,4} and create a new idx_link_sharad_mro

        %% Time window with INUTILE
        %i) the right mro configuration 
        % ii) when sharad is on
        % iii) true anomalies if fl==1
        % iv) solar burst if ok

        %%%%% already checked if time common and time sharad common ==0
        % time_common = time_UTC(idx_time_common);
        % time_sharad_common=sharad_time_UTC(i_sharad);
        % time_common-time_sharad_common=0;

        % Time interval= start time | end time | colatitude rotated | lenght of the
        % time window

        % gia' calcolato
    %     [TimeInterval_1, start_time_1, end_time_1] = find_time_interval(idx_time_common,...
    %         diff_zx_rotated_deg,time_UTC);

        % time window 2 = srat time | end time | time window length | link indeces | links
        % sharad_time_UTC = time_UTC(idx_plt_sharad);

        % 
        % sharad_time_UTC = time_UTC(idx_plt_sharad);
        % [TimeInterval_3, start_time_3, end_time_3] = find_time_interval(i_sharad,...
        %     idx_indice_link,sharad_time_UTC);

        %% List of LBL links
        if ~isempty(idx_link_sharad_mro)

            num_rdr = unique(idx_link_sharad_mro);

            SharadLinkLBL = cell(length(num_rdr),1);
            SharadLinkStart = cell(length(num_rdr),1);
            SharadLinkEnd = cell(length(num_rdr),1);
            SharadIDs = cell(length(num_rdr),1);

            for i_rdr=1:length(num_rdr)
                idx_sharad_rdr = num_rdr(i_rdr);
                SharadLinkLBL(i_rdr)=link_lbl_analysis(idx_sharad_rdr);
                SharadLinkStart(i_rdr)= cellstr(start_time_sharad_analysis(idx_sharad_rdr));
                SharadLinkEnd(i_rdr)= cellstr(end_time_sharad_analysis(idx_sharad_rdr));
                SharadIDs(i_rdr)=num2cell(idx_sharad_rdr);
            end
        else
            disp('No right configuration here')
            fl_no_sharad=1;
            num_rdr=0;
            SharadLinkLBL=[];
            TimeInterval_colatitude=[];
        end



    else

        disp('No SHARAD data here')
        fl_no_sharad=1;
        num_rdr=0;
        SharadLinkLBL=[];
        TimeInterval_colatitude=[];
    end

    %% save data
    if ~isempty(num_rdr) && fl_no_sharad==0

        %%% SAVE OutVariables.mat
        colnames = {'StartTime' 'EndTime' 'TimeWindowLength' 'SharadLinksIDs' 'SharadLinks'};
        SharadPassiveSoundingOpportunities = cell2table( TimeInterval_Link, 'VariableNames', colnames);
        
        save('OutVariables.mat','timeWindow_mro_UTC','num_rdr','SharadPassiveSoundingOpportunities','TimeInterval_colatitude');%, 'start_time', 'end_time')
        
        if fl_solar_probe==2 && ~isempty(solar_burst_table)
            
            %solar_burst_tab=array2table(solar_burst_table,'VariableNames',{'rdr_link', 'peak_time', 'peak_width', 'pk_power', 'diff_sc_mars_colat', 'diff_sc_mars_long','mean_rotated_colat'});
            save('Solar_burst_details.mat', 'solar_burst_table');
            
            % solar_burst_table(size(solar_burst_table,1)+1,:) = [rdr_id solar_burst(i_burst).time solar_burst(i_burst).width solar_burst(i_burst).pks diff_colat diff_long];
        end
        % csv
        writetable(SharadPassiveSoundingOpportunities,'TimeWindows.csv','Delimiter',';');

        %%% SAVE SharadLinks.csv
        SharadLink2 = cell(4,4);

        SharadLink2{2,1}=['StartTime = ' timeWindow_mro_UTC(1,:) ';'];
        SharadLink2{3,1}=['EndTime = ' timeWindow_mro_UTC(2,:) ';'];
        if num_rdr==0
            SharadLink2{4,1}= 'Num_rdr = 0;';
        else
            SharadLink2{4,1}=['Num_rdr = ' num2str(length(num_rdr)) ';'];
        end
        SharadLink2{1,1}=['MROKernel = ' mro_kernel_file ';']; 

        SharadLink2{5,1}='SharadIDs'; SharadLink2{5,2}='SharadLinkLBL';
        SharadLink2{5,3}='SharadLinkStartTime'; SharadLink2{5,4}='SharadLinkEndTime';

        %%%% link sharad | time start sharad | time end sharad
    %     sharad_data{:,1} = SharadIDs;
    %     sharad_data{:,4} = SharadLinkLBL;
    %     sharad_data{:,2}= SharadLinkStart;
    %     sharad_data{:,3}= SharadLinkEnd;  

        SharadLink3=horzcat(SharadIDs, SharadLinkStart,SharadLinkEnd,SharadLinkLBL);
        SharadLink4 = vertcat(SharadLink2, SharadLink3);

        %SharadLink2 = [SharadLink2; sharad_data];
        SharadLinkTable = cell2table(SharadLink4);
        writetable(SharadLinkTable,'SharadLinks.csv','Delimiter',';');

        %writetable(SharadLinkLBL,'OutVariables.csv','Delimiter',';');
        
        if num_rdr==0
            disp('num rdr = 0')
        else
            disp(['num rdr = ' num2str(length(num_rdr))])
            n_rdr_tot= n_rdr_tot+length(num_rdr);
        end
       
        disp(['num tot rdr = ' num2str(n_rdr_tot)])
    else
        num_rdr=0;

        %%%%% SAVE OutVariables.mat
        save('OutVariables.mat','timeWindow_mro_UTC','num_rdr');%, 'start_time', 'end_time')


        %%%%% SAVE SharadLinks.csv
        SharadLink2 = cell(4,1);

        SharadLink2{2}=['StartTime = ' timeWindow_mro_UTC(1,:) ';'];
        SharadLink2{3}=['EndTime = ' timeWindow_mro_UTC(2,:) ';'];
        SharadLink2{4}=['Num_rdr = ' num2str(length(num_rdr)) ';'];
        SharadLink2{1}=['MROKernel = ' mro_kernel_file ';']; 
        SharadLinkTable = cell2table(SharadLink2);
        writetable(SharadLinkTable,'SharadLinks.csv','Delimiter',';');

        disp(['num rdr = ' num2str(num_rdr)])

    end
end

%disp('done')

