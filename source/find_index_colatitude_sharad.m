function [link_lbl_analysis,idx_indice_link,idx_plt_sharad,diff_zx_rotated_deg_idx_plt_sharad,start_time_sharad_analysis, end_time_sharad_analysis] = find_index_colatitude_sharad(diff_zx_rotated_deg, time_UTC, time_interval,start_time_sharad, end_time_sharad, link_lbl)

Time_dt = datetime(time_UTC,'InputFormat','yyyy MMM dd HH:mm:ss.S'); % 2005-08-01 00:00:00.000000 UTC

%% find sharad time in the analysis time interval
[idx_sharad_analysis_start,~] = find_closest_value_in_array(Time_dt(1), start_time_sharad);
[idx_sharad_analysis_end,~] = find_closest_value_in_array(Time_dt(end), start_time_sharad);

while start_time_sharad(idx_sharad_analysis_start) < Time_dt(1)
    disp(['problema start ' datestr(Time_dt(1)) '-' datestr(Time_dt(end))])
    %keyboard
    idx_sharad_analysis_start = idx_sharad_analysis_start+1;
    %start_time_sharad(idx_sharad_analysis_start)
    
end

while end_time_sharad(idx_sharad_analysis_end) > Time_dt(end)
    disp(['problema end ' datestr(Time_dt(1)) '-' datestr(Time_dt(end))])
    %keyboard
    idx_sharad_analysis_end = idx_sharad_analysis_end-1;

end

start_time_sharad_analysis = start_time_sharad(idx_sharad_analysis_start:idx_sharad_analysis_end);
end_time_sharad_analysis = end_time_sharad(idx_sharad_analysis_start:idx_sharad_analysis_end);

link_lbl_analysis = link_lbl(idx_sharad_analysis_start:idx_sharad_analysis_end);

%% find the idx and the value of colatitude for each sharad acquisition
[idx_indice_link,idx_plt_sharad, diff_zx_rotated_deg_idx_plt_sharad] = ...
    plot_colatitude_SHARAD(diff_zx_rotated_deg, time_UTC, time_interval, start_time_sharad_analysis, end_time_sharad_analysis);

end