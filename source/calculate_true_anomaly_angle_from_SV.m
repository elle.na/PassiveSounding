% calculate true anomaly angle from state vectors
% input = state vectors


function [true_anomaly_deg] = calculate_true_anomaly_angle_from_SV(target, et, frame, abcorr, observer)
 

 [ state_vector ] = cspice_spkezr( target, et, frame, abcorr, observer);%km
 
 velocity_vector = state_vector(4:6,:);
 position_vector = state_vector(1:3,:);
 
 velocity_magnitude = sum(abs(velocity_vector).^2).^(1/2);
 position_magnitude = sum(abs(position_vector).^2).^(1/2);
 
 mu = cspice_bodvrd( observer, 'GM', 1 );
 
 ecc_1 =  ((velocity_magnitude.^2)/ mu(1) - (position_magnitude.^(-1)) ) .* position_vector;
 ecc_2 = (dot(position_vector,velocity_vector) / mu(1)) .* velocity_vector;
 eccentricity_vector = ecc_1 - ecc_2;
 
 eccentricity_magnitude = sum(abs(eccentricity_vector).^2).^(1/2);
 
 elts = cspice_oscelt( state_vector, et, mu(1) );
 ecc = elts(2,:);
 
 cos_true_anomaly = dot(eccentricity_vector, position_vector) ./ (eccentricity_magnitude .* position_magnitude);
 true_anomaly_rad = acos(cos_true_anomaly);
 
 if dot(position_vector, velocity_vector)<0
     true_anomaly_rad = 2*pi -true_anomaly_rad;
 end
 
 true_anomaly_deg= rad2deg(true_anomaly_rad);
 