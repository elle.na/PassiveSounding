% rotating the bodies to have the source on the z+ axis, mars in the origin

function [diff_zx_new,radius_MRO,date_MRO]=find_colatitude_angle_csv(dir_path_csv, fn_file,fl_rs,fl_video, step_char)


opts = detectImportOptions([dir_path_csv fn_file{1}], 'Delimiter', ',','VariableNamingRule','preserve');
table_file_JUP = readtable([dir_path_csv fn_file{1}],opts);
clear opts
table_file_JUP(end-3:end ,:)=[];

opts = detectImportOptions([dir_path_csv fn_file{2}], 'Delimiter', ',','VariableNamingRule','preserve');
table_file_MRO = readtable([dir_path_csv fn_file{2}],opts);
clear opts
table_file_MRO(end-5:end ,:)=[];

% check size of the tables
if size(table_file_MRO,1)> size(table_file_JUP,1)
    
    extra = size(table_file_MRO,1)-size(table_file_JUP,1);
    table_file_MRO(end-extra:end,:)=[];
    
elseif size(table_file_MRO,1)< size(table_file_JUP,1)
    
    extra = abs(size(table_file_MRO,1)-size(table_file_JUP,1));
    table_file_JUP(end-extra:end,:)=[];
    
end

if fl_rs==0       % rect coordinate
    k=1;

    figure;
    scatter3(table_file_JUP.X_km_(k),table_file_JUP.Y_km_(k),table_file_JUP.Z_km_(k) )
    hold on
    scatter3(0,0,0, 'red')
    hold on
    scatter3(table_file_MRO.X_km_(k),table_file_MRO.Y_km_(k),table_file_MRO.Z_km_(k),'+', 'black' )

else % spherical coordinate
    
    rho_J=1; 
%     rho_J = table_file_JUP.("Radius (km)");
    phi_J_deg = table_file_JUP.("Colatitude (deg)");
    theta_J_deg = table_file_JUP.("Longitude (deg)");
    
    phi_J = deg2rad(phi_J_deg);
    theta_J = deg2rad(theta_J_deg);
    
    x_J = rho_J.*sin(phi_J).*cos(theta_J);
    y_J = rho_J.*sin(phi_J).*sin(theta_J);
    z_J = rho_J.*cos(phi_J);
        
    rho_MRO=0.5;
    radius_MRO=table_file_MRO.("Radius (km)");
    phi_MRO_deg = table_file_MRO.("Colatitude (deg)");
    theta_MRO_deg = table_file_MRO.("Longitude (deg)");
    
    phi_MRO=deg2rad(phi_MRO_deg);
    theta_MRO=deg2rad(theta_MRO_deg);
    
    x_MRO = rho_MRO.*sin(phi_MRO).*cos(theta_MRO);
    y_MRO = rho_MRO.*sin(phi_MRO).*sin(theta_MRO);
    z_MRO = rho_MRO.*cos(phi_MRO);
    date_MRO = table_file_MRO.("UTC calendar date");
    
    
    %fl_video=0;
    if fl_video ==1
        filename = 'planets.gif';
        scatter_plot_planets(filename, x_J, y_J, z_J,date_MRO, x_MRO, y_MRO, z_MRO); 
    end

    %% angular separation
    % angular separation xy -> theta
    diff_xy = theta_MRO - theta_J ;
    figure;
    plot(diff_xy);
    title(['Angular separation on the xy plane (colatitude) from ' date_MRO{1} ' to ' date_MRO{end}]);
    xlabel(['Time with steps of ' step_char]);
    ylabel('Longitude in deg')
    
    % angular separation zx -> phi
    diff_zx = phi_MRO - phi_J;
    figure;
    plot(diff_zx)
    title(['Angular separation on the zx plane (colatitude) from ' date_MRO{1} ' to ' date_MRO{end}]);
    xlabel(['Time with steps of ' step_char]);
    ylabel('Colatitude in deg')
    
    rho_MRO_real = table_file_MRO.("Radius (km)");
    h_MRO = rho_MRO_real - 3389.5;
    % TO DO: improve the radius calculation
    
    %% axis rotations
    MRO_coord_new_sph = zeros(length(x_MRO),3);
    MRO_coord_new_rec = zeros(length(x_MRO),3);
    J_coord_new_sph= zeros(length(x_MRO),3);
    for k=1:length(x_MRO)

        MRO_coord=[x_MRO(k); y_MRO(k); z_MRO(k)];
        J_coord = [x_J(k); y_J(k); z_J(k)];

        J_coord_new = [0;0;1];
        R=vrrotvec(J_coord,J_coord_new);
        matr=vrrotvec2mat(R);

        J_new = matr*J_coord;
        MRO_coord_new = matr*MRO_coord;
        
        MRO_coord_new_rec(k,:) = MRO_coord_new;

        rho_MRO_new = sqrt(MRO_coord_new(1)^2 +MRO_coord_new(2)^2+MRO_coord_new(3)^2);
        phi_MRO_new = atan(MRO_coord_new(2)/MRO_coord_new(1));
        theta_MRO_new = atan(sqrt(MRO_coord_new(1)^2 + MRO_coord_new(2)^2)/MRO_coord_new(3));

        MRO_coord_new_sph(k,:) = [rho_MRO_new rad2deg(phi_MRO_new) rad2deg(theta_MRO_new)];

        rho_J_new = 1;%sqrt(J_new(1)^2 +J_new(2)^2+J_new(3)^2);
        phi_J_new = 0;%atan(J_new(2)/J_new(1));
        theta_J_new = 0;%atan(sqrt(J_new(1)^2 +J_new(2)^2)/J_new(3));

        J_coord_new_sph(k,:) = [rho_J_new rad2deg(phi_J_new) rad2deg(theta_J_new)];
  
    end
    
    %fl_video=0;
    if fl_video ==1
        filename='planets_rotated.gif';
        scatter_plot_planets(filename, J_coord_new_sph(:,2), J_coord_new_sph(:,3),J_coord_new_sph(:,1),date_MRO, MRO_coord_new_rec(:,1), MRO_coord_new_rec(:,2), MRO_coord_new_rec(:,3)); 
    end

    
    %% angular separation
    % angular separation xy -> theta
    diff_zx_new =MRO_coord_new_sph(:,2);
    figure;
    plot(diff_zx_new);
    title(['Angular separation on the rotated zx plane (colatitude) from ' date_MRO{1} ' to ' date_MRO{end}]);
    xlabel(['Time with steps of ' step_char]);
    ylabel('Colatitude in deg')
    
end

end