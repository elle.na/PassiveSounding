function [IdxPlot_SharadOn,DiffAngle_SharadOn]=plot_agle_MRO_SHARAD(diff_zx_rotated_deg,idx_plt,val_idx_plt,fl_plot,time_UTC,start_time_sharad, end_time_sharad)

% idx_start = zeros(1,length(start_time_sharad));
% idx_end = zeros(1,length(start_time_sharad));

idx_start =[];
idx_end = [];
DiffAngle_SharadOn=[];
IdxPlot_SharadOn = [];

for i = 1:length(start_time_sharad)
    [idx_tmp_s,v_s]=find_closest_value_in_array(start_time_sharad(i), time_UTC);
    [idx_tmp_e,v_e]=find_closest_value_in_array(end_time_sharad(i), time_UTC);
    
    if v_s <= abs(start_time_sharad(2)-start_time_sharad(1)) 
%         idx_start_UTC(length(idx_start)+1) = idx_tmp_s;
%         idx_end_UTC(length(idx_start)+1) = idx_tmp_e;
%         
        temp = diff_zx_rotated_deg(idx_tmp_s:idx_tmp_e);
        DiffAngle_SharadOn = [DiffAngle_SharadOn temp];
        
        temp_idx = idx_tmp_s:1:idx_tmp_e;
        IdxPlot_SharadOn = [IdxPlot_SharadOn temp_idx];
        
        clear temp      
    end
      
end


if fl_plot==1
    figure; 
    
    plot(diff_zx_rotated_deg);
    hold on
    plot(idx_plt, val_idx_plt,'r.'); % angle ok
    hold on
    % sharad on
    plot(idx_plt, DiffAngle_SharadOn,'y.'); % angle ok
    hold off
    
    title(['Angular separation on the rotated zx plane (colatitude) from ' time_UTC{1} ' to ' time_UTC{end}]);
    xlabel(['Time with steps of ' num2str(time_interval) ' s']);
    ylabel('Colatitude in deg')

end