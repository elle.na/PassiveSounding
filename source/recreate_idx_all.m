function [idx_all] = recreate_idx_all(TimeInterval_Link)

idx_all=[];
for i_tw = 1:size(TimeInterval_Link,1)
    idx_all =  [idx_all TimeInterval_Link{i_tw,4}];
end