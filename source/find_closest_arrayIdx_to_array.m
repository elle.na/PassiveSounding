% To compute the closest value in a vector “N” for each element of “V”

function [closestValue,closestIndex]=find_closest_arrayIdx_to_array(N,V)

A = repmat(N,[1 length(V)]);
[minValue,closestIndex] = min(abs(A-V'));
closestValue = N(closestIndex) ;