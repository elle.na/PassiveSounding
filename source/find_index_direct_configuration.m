function [idx_plt,diff_zx_rotated_deg_idx_plt] = find_index_direct_configuration(diff_zx_rotated, time_UTC, time_interval,fl_plot,angle_deg, delta_theta_star)

% rough calculation

diff_zx_rotated_deg = diff_zx_rotated;

th_min = angle_deg - delta_theta_star;
th_max = angle_deg + delta_theta_star;

[idx] = find(diff_zx_rotated_deg <= th_max);
tmp=diff_zx_rotated_deg(idx);
[idx2] = find(tmp >= th_min);
diff_zx_rotated_deg_idx_plt = tmp(idx2);
idx_plt = idx(idx2);

if fl_plot==1
    figure; 
    
    plot(diff_zx_rotated_deg);
    hold on
    plot(idx_plt, diff_zx_rotated_deg_idx_plt,'r.');

    hold off
    
    title(['Angular separation on the rotated zx plane (colatitude) from ' time_UTC{1} ' to ' time_UTC{end}]);
    xlabel(['Time with steps of ' num2str(time_interval) ' s']);
    ylabel('Colatitude in deg')

end


end