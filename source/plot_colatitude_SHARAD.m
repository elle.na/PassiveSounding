function [idx_indice_link,idx_plt_sharad, diff_zx_rotated_deg_idx_plt_sharad] = plot_colatitude_SHARAD(diff_zx_rotated_deg, time_UTC, time_interval, SharadStartTime, SharadEndTime)

% load('C:\Users\admin\Desktop\GIT\PassiveSounding\spec_theta(h).mat','theta_sc_star','hh');
% theta_sc_star = double(theta_sc_star);

% %% rough calculation colatitude angle ok
% theta_sc_star_deg = double(rad2deg(theta_sc_star));
% th_min = min(theta_sc_star_deg(:))-6;
% th_max = max(theta_sc_star_deg(:))+6;
% 
% [idx] = find(diff_zx_rotated_deg <= th_max);
% tmp=diff_zx_rotated_deg(idx);
% [idx2] = find(tmp >= th_min);
% diff_zx_rotated_deg_idx_plt = tmp(idx2);
% idx_plt = idx(idx2);


%% idx sharad active with mro with right configuration
time_UTC_dt = datetime(cell2mat(time_UTC), 'Format', 'yyyy MMM dd HH:mm:ss.S');

idx_plt_sharad=[];
diff_zx_rotated_deg_idx_plt_sharad=[];
idx_indice_link=[];

for i_sharad = 1: length(SharadStartTime)
    
    [idx_start,~]=find_closest_value_in_array(SharadStartTime(i_sharad), time_UTC_dt);
    [idx_end,~]=find_closest_value_in_array(SharadEndTime(i_sharad), time_UTC_dt);
    
    th= duration([0 10 0]);
    if abs(SharadEndTime(i_sharad)-time_UTC_dt(idx_end)) > th || abs(SharadStartTime(i_sharad) - time_UTC_dt(idx_start))> th
       disp('problema')
       keyboard
        
    end
    
    idx_plt_sharad = [idx_plt_sharad idx_start:idx_end];
    idx_indice_link =[idx_indice_link i_sharad*ones(size(idx_start:idx_end))];
    diff_zx_rotated_deg_idx_plt_sharad =[diff_zx_rotated_deg_idx_plt_sharad diff_zx_rotated_deg(idx_start:idx_end)];

end


%% plot
figure; 

plot(diff_zx_rotated_deg);
hold on
% plot(idx_plt, diff_zx_rotated_deg_idx_plt,'r.');
% hold on

plot(idx_plt_sharad, diff_zx_rotated_deg_idx_plt_sharad,'g.');

hold off

title(['Angular separation on the rotated zx plane (colatitude) from ' time_UTC{1} ' to ' time_UTC{end}]);
xlabel(['Time with steps of ' num2str(time_interval) ' s']);
ylabel('Colatitude in deg')

savefig('AngularSeparation_Sharad.fig')



