% path= 'C:\Users\admin\Desktop\GIT\PassiveSounding\source\IDL reader\';
% fname='20040113.R2';
function [nb_burst, solar_burst]= analyze_dynamic_specra_wind(date,fl_plot)

addpath(genpath('C:\Users\admin\Desktop\GIT\PassiveSounding\source\IDL reader'));
fname = [date '.R2'];

link_wind = 'https://solar-radio.gsfc.nasa.gov/data/wind/rad2/';

link_date = [date(1:4) '/rad2/' fname ];
link = [link_wind link_date];

%https://solar-radio.gsfc.nasa.gov/data/wind/rad2/2003/rad2/20030101.R2


outfilename = websave(fname,link);


% read R2 file
outargs=restore_idl(outfilename,'verbose');

% stereo
% spectra = outargs.SPECTRUM;
% spectra = flipud(spectra);
% spectra(:, end)=[];
% %spectra = normalize(spectra, 'range', [0 1]);
% figure; imagesc(spectra); colorbar;
% %caxis([-10 max(spectra(:))])


% wind
spectra = (outargs.ARRAYB(:,:))';
spectra = flipud(spectra);
spectra(:, end)=[];

%N_av = 3;
H_LP = fspecial('average',[3 2 ])';
spectra_LP = imfilter(spectra,H_LP);

spectra_db = pow2db(spectra_LP);
spectra_db(spectra_db==-Inf)= 200;
spectra_db(spectra_db==200)= min(spectra_db(:));
%spectra_db(spectra_db<0)=mean_tot;
% figure; imshow(spectra_db); colorbar; colormap('parula');

% mean
mean_tot = mean(spectra_db(:));
sdt_tot = std(spectra_db(:));
%spectra_db_norm=(spectra_db-mean_tot)/sdt_tot;

spectra_db(spectra_db<0) = 0; %!!!! mean tot o 0?
%mean_spectra_norm = mean(spectra_db_norm);
mean_spectra_tot = mean(spectra_db(:));

spectra_db(spectra_db() < mean_spectra_tot) = mean_spectra_tot;
mean_spectra = mean(spectra_db);
%mean_spectra_tot = mean(spectra_db(:));
% figure; 
% plot(mean_spectra)
% hold on
% plot(mean_spectra_norm)
% hold off

x_time = cumsum(ones(size(mean_spectra)))/60; % ore
y_freq = linspace(13.82, 1.075, size(spectra_db,1));

th_peak = sdt_tot/2;

if th_peak==-Inf
    nb_burst=0;
    solar_burst=[];
else
    % disp(th_peak)

    if fl_plot==1
        str_date = fname(1:end-2);

        figure;
        sb(1)=subplot(211);
        imagesc(x_time, y_freq,spectra_db);  colormap('jet');
        caxis([0.5 2])
    %     a=colorbar;
    %     a.Label.String = 'Normalized power (dB)';
        ylabel('Frequency (MHz)')
        xlabel('Time (hour)')
        set(gca,'ytick',[])
        title(['Dynamic spectra' str_date])

        sb(2)=subplot(212);

        plot(x_time,mean_spectra)
        hold on
        findpeaks(mean_spectra,'MinPeakProminence',th_peak)
        % hold on
        % plot(mean_spectra_norm)
        hold off
        ylabel('Normalized Power (dB)')
        xlabel('Time [min]')
        xlim([1 length(mean_spectra)])
        %set(gca,'ytick',[])
        title('Average dynamic spectra')

        linkaxes(sb,'x')

        fn_fig = ['Dynamic_spectra_' str_date];
        savefig(fn_fig)

    end

    [pks,locs,w,~] = findpeaks(mean_spectra,'MinPeakProminence',th_peak);

    if ~isempty(pks)
        nb_burst=length(pks);

        for i_burst =1:nb_burst
            minu=locs(i_burst);
            solar_burst_time = minutes(minu);
            solar_burst_time.Format = 'hh:mm:ss';
            solar_burst_time = datetime([date '000000'],'InputFormat','yyyyMMddhhmmss') +solar_burst_time;
            % t = datetime(DateStrings,'InputFormat','yyyy-MM-dd')

            solar_burst_width = minutes(ceil(w));
            %solar_burst_width;

            solar_burst(i_burst).time = solar_burst_time;
            solar_burst(i_burst).width = solar_burst_width;
            solar_burst(i_burst).pks = pks(i_burst);
        end

    else

        nb_burst=0;
        solar_burst=[];

    end
    % disp(nb_burst)
    
end


end
