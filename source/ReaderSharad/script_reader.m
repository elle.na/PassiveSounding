function signal = script_reader(file_name1)
%-------------------------------------------------------------------------%
% Script that aims to read Sharad RDR radargrams
%-------------------------------------------------------------------------%

%
% Radargram 1
% name_rad = 'E:\UniTrento\Working with radargrams\rdr read script\rdr data\mrosh_1001\data\rdr04xxx\rdr0405101\r_0405101_001_ss19_700_a.dat';
%  
% file_name1 = strcat(name_rad);  %read the data file
A = readsharadrdr(file_name1,'EchoSamplesReal'); %extract real part of the signal
B = readsharadrdr(file_name1,'EchoSamplesImaginary'); %extract imaginary part of the signal
signal = A + 1i*B;

shift = readsharadrdr(file_name1,'RangeShift');
[~,N_y] = size(signal);
for i=1:N_y
    signal(:,i) = circshift(signal(:,i),-round(double(shift(i)/2)));  %shift each range line for matching
end
module_Z = abs(signal);

%enviwrite((20.*log10(signal))',[name_rad 'left']);
% figure
% imagesc(module_Z);
% colormap('Gray')
% title('Full SHARAD radargram')
%imagesc(angle(signal));


end

