function [delta_T] = find_time_stereo_mars(time_UTC)

[et] = cspice_str2et( datestr(time_UTC ));

speed_light = 299792.458; % km/s

frame    = 'IAU_SUN';
abcorr   = 'LT+S';
observer = 'SUN';

%% distance SUN - MARS
% rect coordinate
target_sc   = 'STEREO Ahead';
[ state_rect_stereo ] = cspice_spkpos( target_sc, et, frame, abcorr, observer);%km

% convert in spherical
[r, colat, lon] = cspice_recsph(state_rect_stereo); %rad
state_sc_sph_stereo = [r; colat; lon];

%% distance SUN STEREO
% rect coord
target_sc   = 'MARS';
[ state_rect_mars ] = cspice_spkpos( target_sc, et, frame, abcorr, observer);%km
% spherical coord
[r, colat, lon] = cspice_recsph(state_rect_mars); %rad
state_sc_sph_mars = [r; colat; lon];

%% distance mars - stereo
% take the radius of mars - stereo

distance_mars_stereo =  abs(state_sc_sph_mars(1) - state_sc_sph_stereo(1));

%% convert distance (in km) to time
delta_T = distance_mars_stereo/speed_light; % secondi


