clc
clear
close all

c  = 299792458;
R = 3389.5e3;

% sharad
hnlow  = 255e3;
hnhigh = 320e3;

%marsis
orbitlow = 250e3;
orbithigh = 900e3;

h = hnlow;


%---

psi_i= 0:0.001:pi/2;

theta_sc = 2*psi_i - sin(psi_i)/(1+h/R);

r1 = -R*cos(psi_i);
r2 = sqrt( (R+h)^2 + R^2 - 2*R*(R+h)*cos(theta_sc-psi_i)  );
r3 = - (R+h)*cos(theta_sc);

tr = (r1+r2-r3)/c;

figure
plot(theta_sc*180/pi, tr*1e6, 'LineWidth', 2)
xlim([0 120])
xlabel('\theta_{sc} [deg]')
ylabel('t_r [\mu s]')
title('SHARAD, h=255km')
grid on


figure
plot(psi_i*180/pi, tr*1e6, 'LineWidth', 2)
xlabel('\psi_{i} [deg]')
ylabel('t_r [\mu s]')
title('SHARAD, h=255km')
grid on

%% ---


% theta_of_h = @(hh,psi) ( -R*cos(psi) ...
%     + sqrt((R+hh).^2 + R^2 - 2.*R.*(R+hh).*cos(psi - sin(psi)./(1+hh/R)))...
%     + (R+hh).*cos(2*psi - sin(psi)./(1+hh/R)) )/c - 1428e-6
% 
% figure
% fimplicit(theta_of_h,[255e3 320e3 0 pi/2])


hh=255e3:1000:320e3;
for i = 1 : length(hh)
    hhh=hh(i);
    
    syms psi
    theta_of_hhh = ( -R*cos(psi) ...
    + sqrt((R+hhh).^2 + R^2 - 2.*R.*(R+hhh).*cos(psi - sin(psi)./(1+hhh/R)))...
    + (R+hhh).*cos(2*psi - sin(psi)./(1+hhh/R)) )/c == 1428e-6;

    S = vpasolve(theta_of_hhh, psi, [0,pi/2]);
    theta_sc_star(i) = (2*S - sin(S)/(1+h/R));
end

figure
plot(hh/1e3, theta_sc_star*180/pi, 'LineWidth', 2)
title('Specular oblique sounding')
xlabel('SHARAD altitude [km]')
ylabel('Angle \theta* needed for perfect overlap [deg]')
grid on

save('spec_theta(h).mat', 'hh', 'theta_sc_star')