function plot2d_planets_MRO_STEREO( date_MRO )

frame    = 'IAU_SUN';
abcorr   = 'LT+S';
observer = 'SUN';

[et] = cspice_str2et( datestr(date_MRO));


% Mars
[ state_source_rec_mars ] = cspice_spkpos( 'MARS', et, frame, abcorr, observer); %km
% [ state_source_rec ] = cspice_spkezr( target_source, time_array, frame, abcorr, observer);%km

[~, colat_mars, lon_mars] = cspice_recsph(state_source_rec_mars);
%rad
r_mars=3;
%state_source_sph = [r; colat; lon]; %rad

mars_rect = cspice_sphrec(3 , colat_mars, lon_mars);

%%%% stereo
target_sc   = 'STEREO Ahead';
[ state_rect_stereo ] = cspice_spkpos( target_sc, et, frame, abcorr, observer);%km

[~, colat_stereo, lon_stereo] = cspice_recsph(state_rect_stereo); %rad
%state_source_sph = [r; colat; lon]; %rad

stereo_rect = cspice_sphrec(1 , colat_stereo, lon_stereo);
r_stereo=1;


% Earth
[ state_source_rec_earth ] = cspice_spkpos( 'EARTH', et, frame, abcorr, observer); %km
% [ state_source_rec ] = cspice_spkezr( target_source, time_array, frame, abcorr, observer);%km

[~, colat_earth, lon_earth] = cspice_recsph(state_source_rec_earth);
%rad
r_earth=2;
%state_source_sph = [r; colat; lon]; %rad

earth_rect = cspice_sphrec(2 , colat_earth, lon_earth);


%%% mro
target_sc   = 'MRO';
[ state_rect_mro ] = cspice_spkpos( target_sc, et, frame, abcorr, observer);%km

[~, colat_mro, lon_mro] = cspice_recsph(state_rect_mro); %rad
%state_source_sph = [r; colat; lon]; %rad

mro_rect = cspice_sphrec(2 ,  colat_mro, lon_mro);
r_mro=2;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f1 = figure;
%axes('position',[0 0 1 1]);

polarscatter(0,0,'filled',  'yellow'); %sun
hold on
polarscatter(colat_mars, r_mars,'filled',  'red');
hold on
polarscatter(colat_earth, r_earth ,'filled', 'green');
hold on
polarscatter(colat_stereo, r_stereo, 'filled', 'blue')
title(['Colatitude ' datestr(date_MRO)])
legend('Sun', 'Mars', 'Earth', 'Stereo-A','Location','southoutside','NumColumns',3);
% rticks([])
rticklabels({'','','',''})
hold off

fn=regexprep(datestr(date_MRO), ' ', '_');
fn=regexprep(fn, ':', '');

saveas(f1, ['plot_colat_sun_mars_mro_' fn '.png']);
savefig(f1, ['plot_colat_sun_mars_mro_' fn '.fig'])
close all


f2 = figure;
%axes('position',[0 0 1 1]);

polarscatter(0,0,'filled', 'yellow'); %sun
hold on
polarscatter(lon_mars, r_mars,'filled', 'red');
hold on
polarscatter(lon_earth, r_earth,'filled' ,'green');
hold on
polarscatter(lon_stereo, r_stereo, 'filled','blue')
% rticks([])
rticklabels({'','','',''})
title([ 'Longitude ' datestr(date_MRO)])

legend('Sun', 'Mars', 'Earth', 'Stereo-A','Location','southoutside','NumColumns',3);

hold off

saveas(f2, ['plot_long_sun_mars_earth_stereo_' fn '.png']);
savefig(f2, ['plot_long_sun_mars_earth_stereo_' fn '.fig'])




f1 = figure;
%axes('position',[0 0 1 1]);

polarscatter(0,0,'filled',  'yellow'); %sun
hold on
polarscatter(colat_mars, r_mars,'filled',  'red');
hold on
polarscatter(colat_mro, r_mro ,'filled', 'green');

title(['Colatitude ' datestr(date_MRO)])
legend('Sun', 'Mars', 'MRO','Location','southoutside','NumColumns',3);
% rticks([])
rticklabels({'','','',''})
hold off

fn=regexprep(datestr(date_MRO), ' ', '_');
fn=regexprep(fn, ':', '');

saveas(f1, ['plot_colat_sun_mars_mro_' fn '.png']);
savefig(f1, ['plot_colat_sun_mars_mro_' fn '.fig'])
close all


f2 = figure;
%axes('position',[0 0 1 1]);

polarscatter(0,0,'filled', 'yellow'); %sun
hold on
polarscatter(lon_mars, r_mars,'filled', 'red');
hold on
polarscatter(lon_mro, r_mro,'filled' ,'black');


% rticks([])
rticklabels({'','','',''})
title([ 'Longitude ' datestr(date_MRO)])

legend('Sun', 'Mars', 'MRO','Location','southoutside','NumColumns',3);

hold off

saveas(f2, ['plot_long_sun_mars_mro_' fn '.png']);
savefig(f2, ['plot_long_sun_mars_mro_' fn '.fig'])


