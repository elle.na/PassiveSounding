function [TimeInterval,start_time, end_time] = find_time_interval(idx_plt,diff_zx_rotated_deg,time_UTC)

start_time = [];
end_time = [];

TimeInterval = {};

i_TI=1; i_time = 1;

% t = cell2mat(time_UTC);
% t(:,end-1:end)=[];
% t = num2cell(t,2);
Time_dt = datetime(time_UTC,'InputFormat','yyyy MMM dd HH:mm:ss.S'); % 2005-08-01 00:00:00.000000 UTC

dt = Time_dt(2)-Time_dt(1);

i_time=1;
while i_time<=length(idx_plt)

 if i_time==length(idx_plt) || idx_plt(i_time)+1 ~=idx_plt(i_time+1) % time non consecutivi

    TimeInterval{i_TI,1} = Time_dt(idx_plt(i_time))-dt/2;
    TimeInterval{i_TI,2} = Time_dt(idx_plt(i_time))+dt/2;
    TimeInterval{i_TI,3} = diff_zx_rotated_deg(idx_plt(i_time));% average angle value
    TimeInterval{i_TI,4} =TimeInterval{i_TI,2}-TimeInterval{i_TI,1};
    
    start_time = [start_time; Time_dt(i_time)-dt/2];
    end_time = [end_time; Time_dt(i_time)+dt/2];

 elseif  i_time < length(idx_plt)   && idx_plt(i_time)+1 == idx_plt(i_time+1) % time consecutivi
    % looking for start
    
    i_init = idx_plt(i_time);
  
    TimeInterval{i_TI,1} = Time_dt(i_init);
    start_time = [start_time; Time_dt(i_init)];
    
   % looking for the end
    while i_time < length(idx_plt) && idx_plt(i_time)+1 == idx_plt(i_time+1)
        i_time = i_time+1;
    end
    
    i_end = idx_plt(i_time);
    TimeInterval{i_TI,2} = Time_dt(i_end);
    end_time = [end_time; Time_dt(i_end)];

    TimeInterval{i_TI,3} = diff_zx_rotated_deg(i_init:i_end);%
    TimeInterval{i_TI,4} = TimeInterval{i_TI,2}-TimeInterval{i_TI,1};
    

 end
 i_TI=i_TI+1;
 i_time=i_time+1;

end

end