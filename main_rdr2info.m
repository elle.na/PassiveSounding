clear
close all
clc

rdr_id = '1913101'; 
% 4202801
% 4243501
%% paths
cd('C:\Users\admin\Desktop\GIT\PassiveSounding')
addpath(genpath('mice'));
addpath(genpath('C:\Users\admin\Desktop\GIT\PassiveSounding\source'));

% dir out
dir_out = ['out\Test18_' rdr_id '\'];
mkdir(dir_out)
addpath(genpath(dir_out));
cd(dir_out)

kernel_path = 'E:\Kernel\';
addpath(genpath(kernel_path));
%% flags
fl_video = 0;
fl_plot = 1;
fl_rough_analysis =0;
fl_no_sharad=0;
fl_true_anomalies_analysis=0;
th_deg_TA=90; %deg
fl_solar_burst_fine = 1;

target_source   = 'SUN'; % JUPITER
target_sc   = 'MRO';
frame    = 'IAU_MARS';
abcorr   = 'LT+S';
observer = 'Mars';
time_interval = 10; % seconds

if strcmp(target_source, 'SUN')
    fl_solar_burst=1;
else
    fl_solar_burst=0;
end


%% extract sharad data id
start_time_column = 11;
end_time_column = 12;
link_column = 3;
nb_file=1;

path_data = 'C:\Users\admin\Desktop\GIT\PassiveSounding\data_sharad';
fn_data = 'cumindex_';

addpath(genpath(path_data));

[start_time_sharad, end_time_sharad, link_lbl] = extract_sharad_times(fn_data,path_data,nb_file,link_column, end_time_column,start_time_column);

idx_link = contains(link_lbl,rdr_id);
index_rdr_sharad = find(idx_link==1);

if max(idx_link)>0

    start_time_sharad_rdr_id = start_time_sharad(index_rdr_sharad);
    end_time_sharad_rdr_id = end_time_sharad(index_rdr_sharad);
    link_lbl_rdr_id = link_lbl(index_rdr_sharad);
    duration_rdr_id = end_time_sharad_rdr_id - start_time_sharad_rdr_id;


    %% kernel and state vector

    % mro kernels
    %kernel_mro_folder = [kernel_path 'mro-psp1_2\'];
    kernel_mro_folder = [kernel_path 'mrosp_1000_070701_151231\mrosp_1000\data\'];
    %D:\Kernel\mrosp_1000_070701_151231\mrosp_1000\data\spk
    % mrosp_1000_151231_211231\mrosp_1000\data\
    % D:\Kernel\mrosp_1000_070701_151231


    load_kernels;

    mro_bsp_files = dir([kernel_mro_folder '\spk\mro_psp??.bsp']);
    n_files = size(mro_bsp_files,1);

    for i_file =1:n_files

        fn_mro_kernel = mro_bsp_files(i_file).name;

        cspice_furnsh([kernel_mro_folder '\spk\' fn_mro_kernel]);

        %% start and ending time
        timeWindow_mro = cspice_spkcov( [kernel_mro_folder 'spk\' fn_mro_kernel], cspice_bodn2c('MRO'), 1e6 );
        format = 'C';
        prec=0;
        timeWindow_mro_UTC = cspice_et2utc( timeWindow_mro', format, prec );
        % size(timeWindow_mro) = [2,1]

    end

    timeWindow_mro_utc = [start_time_sharad_rdr_id ,end_time_sharad_rdr_id];
    timeWindow_mro=cspice_str2et( datestr(timeWindow_mro_utc ));

    [diff_zx_rotated_deg,time_UTC,state_sc_sph,time_array,~]=...
        calculate_state_vectors(target_source,target_sc,frame,abcorr,observer, time_interval,timeWindow_mro,fl_video);

    Time_dt = datetime(time_UTC,'InputFormat','yyyy MMM dd HH:mm:ss.S'); % 2005-08-01 00:00:00.000000 UTC

    close all

    %[link_lbl_analysis,idx_indice_link,idx_plt_sharad,diff_zx_rotated_deg_idx_plt_sharad,start_time_sharad_analysis, end_time_sharad_analysis] =...
    %        find_index_colatitude_sharad(diff_zx_rotated_deg, time_UTC, time_interval,start_time_sharad_rdr_id, end_time_sharad_rdr_id,link_lbl_rdr_id);
    idx_plt_sharad = cumsum(ones(1, length(diff_zx_rotated_deg)));
    diff_zx_rotated_deg_idx_plt_sharad = diff_zx_rotated_deg;

    delta_theta_star = 6; % deg
    [idx_plt,sc_altitude,spoint] = find_index_theta_star(state_sc_sph,observer, time_array, abcorr, target_sc,diff_zx_rotated_deg,delta_theta_star);
    val_idx_plt = diff_zx_rotated_deg(idx_plt);

    %% coordinates
    [radius, longitude, latitude] = cspice_reclat(spoint);
    longitude                     = longitude * cspice_dpr;
    latitude                      = latitude  * cspice_dpr;

    CoordinateMarsNadir.longitude = longitude;
    CoordinateMarsNadir.latitude = latitude;
    CoordinateMarsNadir.radius = radius;

    %% find common indeces bwt sharad and mro

    [idx_time_common, i_sharad, ~] = intersect(idx_plt_sharad, idx_plt,'legacy');

    %[idx_time_common, i_sharad, i_mro] = intersect(idx_plt_sharad, idx_plt,'legacy');
    % idx_time_common = idx_plt_sharad(i_sharad);
    % idx_time_common = idx_plt(i_mro);

    ColatitudeAngleRotated = diff_zx_rotated_deg;
    ColatitudeCommon = diff_zx_rotated_deg(idx_time_common);

    [TimeInterval_colatitude_MRO_SHARAD, start_time_MRO_SHARAD, end_time_MRO_SHARAD] = find_time_interval(idx_time_common, diff_zx_rotated_deg,time_UTC);
     
    %% solarburst analyiss
    if fl_solar_burst
        nb_tw = size(TimeInterval_colatitude_MRO_SHARAD,1);
        TimeInterval_colatitude_MRO_SHARAD_SB=TimeInterval_colatitude_MRO_SHARAD;
        start_time_MRO_SHARAD_SB = start_time_MRO_SHARAD;
        end_time_MRO_SHARAD_SB = end_time_MRO_SHARAD;
    
    
    
        fl_NO_Tw=[]; % tw to remove because there are no solar burst
        
        for i_tw=1:nb_tw
            date_tocheck = start_time_MRO_SHARAD(i_tw);
            duration_tocheck = end_time_MRO_SHARAD(i_tw) - start_time_MRO_SHARAD(i_tw);
                        
            formatOut = 'yyyymmdd';
            date_solar_burst=datestr(date_tocheck,formatOut);
            [nb_burst, solar_burst]= analyze_dynamic_specra_wind(date_solar_burst,fl_plot);
            
            disp([ 'TOT burst numbers: ' num2str(nb_burst)]);
            
            if nb_burst==0 % there are burst
                fl_NO_Tw=[fl_NO_Tw i_tw];   
            else
                if fl_solar_burst_fine == 1
                    fl_bs_ok = 0;
                    for i_burst =1:nb_burst
                        % uncomment if needed
                        % start_burst = solar_burst(i_burst).time - (solar_burst(i_burst).width)/2;
                        % end_burst =solar_burst(i_burst).time + (solar_burst(i_burst).width)/2;
                        
                        
                        
                        mid_tocheck = date_tocheck + duration_tocheck/2;
                        
                        % travelling time L1 to mars
                        [delta_T] = find_time_L1_mars(mid_tocheck);
                        
                        %print(seconds(delta_T))
                        if (mid_tocheck - (solar_burst(i_burst).time + seconds(delta_T)))<= 2*max(duration_tocheck, solar_burst(i_burst).width)
                            fl_bs_ok=1;
                            disp(['burst number ' num2str(i_burst) ', ' num2str(delta_T) ' sec, ' num2str(delta_T/60) ' min'])
                        end
                    end 
                    if fl_bs_ok == 0
                        fl_NO_Tw = [fl_NO_Tw i_tw];
                    end
                end          
                
            end
                     
        end
        
        if ~isempty(fl_NO_Tw)
            TimeInterval_colatitude_MRO_SHARAD_SB(fl_NO_Tw,:)=[];
            start_time_MRO_SHARAD_SB(fl_NO_Tw,:)=[];
            end_time_MRO_SHARAD_SB(fl_NO_Tw,:)=[];
            %TimeInterval_Link(fl_NO_Tw,:)=[];
%         else
%             TimeInterval_colatitude_MRO_SHARAD_SB=TimeInterval_colatitude_MRO_SHARAD;
%             start_time_MRO_SHARAD_SB=start_time_MRO_SHARAD;
%             end_time_MRO_SHARAD_SB=end_time_MRO_SHARAD_SB;
        end

        TimeInterval_colatitude = TimeInterval_colatitude_MRO_SHARAD_SB;
        start_time = start_time_MRO_SHARAD_SB;
        end_time = end_time_MRO_SHARAD_SB;
        
        
        if ~isempty(TimeInterval_colatitude)
            [idx_link_sharad_mro_burst] = recreate_idx_all(TimeInterval_colatitude);
            diff_zx_rotated_deg_idx_burst= diff_zx_rotated_deg(idx_link_sharad_mro_burst);
        else
            idx_link_sharad_mro_burst=[];
        end    
            
    end
        
        
%% PLOT
    if fl_plot==1
        figure;

        % time vs rotated colatitude angle
        plot(diff_zx_rotated_deg);
        hold on

        % val_idx_plt = diff_zx_rotated_deg(idx_plt)
        plot(idx_plt, val_idx_plt,'r.');

        hold on

        diff_zx_rotated_deg_idx_plt_sharad = diff_zx_rotated_deg(idx_plt_sharad);
        plot(idx_link_sharad_mro_burst, diff_zx_rotated_deg_idx_burst,'y.');

        %hold on

        % ColatitudeCommon = diff_zx_rotated_deg(idx_common);
        %plot(idx_time_common, ColatitudeCommon,'g.');

        hold off

        title(['Angular separation on the rotated zx plane (colatitude) from ' time_UTC{1} ' to ' time_UTC{end}]);
        xlabel(['Time with steps of ' num2str(time_interval) ' s']);
        ylabel('Colatitude in deg') 

        legend('Rotated Colatitude','MRO in the oblique configuration', 'SHARAD acquisition')
        legend('Location','southoutside')

        savefig('AngularSeparation_Sharad_MRO.fig')
    end

    save('data.mat','ColatitudeAngleRotated', 'sc_altitude','ColatitudeCommon','rdr_id','CoordinateMarsNadir')

else
   disp('file not found') 
end
    
cd('C:\Users\admin\Desktop\GIT\PassiveSounding')