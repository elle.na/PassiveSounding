clear
close all

cd('C:\Users\admin\Desktop\GIT\PassiveSounding')
addpath(genpath('mice'));
addpath(genpath('C:\Users\admin\Desktop\GIT\PassiveSounding\source'));

dir_path_csv = 'C:\Users\admin\Desktop\GIT\PassiveSounding\coordinate\';


%% Calculate Colatitude angle
fl_rs = 1;
if fl_rs ==1 % coord spherical
    
    % fn_file={'WGC_StateVector_20210825170108.csv','WGC_StateVector_20210825170652.csv'};
    % fn_file={'WGC_StateVector_20210903020917.csv','WGC_StateVector_20210903021515.csv'}; % source, spacecraft
    % fn_file={'WGC_StateVector_20210903021515.csv','WGC_StateVector_20210903020917.csv'};
    % fn_file={'WGC_StateVector_20210922174532.csv','WGC_StateVector_20210922174510.csv'};
    fn_file={'WGC_StateVector_20210922181949.csv','WGC_StateVector_20210922182006.csv'};
    step_char='10s';

else    % coord rect
    
    fn_file = {'WGC_StateVector_20210825164206.csv','WGC_StateVector_20210825165534.csv'};
    
end

fl_video=0;
[colatitude_angle,radius_MRO,Time]=find_colatitude_angle_csv(dir_path_csv, fn_file,fl_rs,fl_video, step_char);
% radius_MRO in km
% colatitude_angle in deg

%% Calculate the theta star for each sc altitude 

load('spec_theta(h).mat')
theta_sc_star = double(theta_sc_star);

radii  = cspice_bodvrd( 'MARS', 'RADII', 3 ); %m
sc_altitude = radius_MRO-Mars_radius;
% TO DO: find a better way to compute the spacecraft altitude
% radius wrt to the sc coordinate
% try: surface intercept point 
% (https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/cspice/subpnt_c.html)

orbit_altitude = (hh/1000)'; % convert m in km

sc_altitude_round = round(sc_altitude);

temp = repmat(orbit_altitude,[1 length(sc_altitude_round)]);
[minValue,closestIndex] = min(abs(temp-sc_altitude_round'));
closestValue = orbit_altitude(closestIndex); 
clear temp

%theta_star_rad = zeros(size(closestValue)); 
theta_star_rad = theta_sc_star(1, closestIndex);

theta_star_deg= rad2deg(theta_star_rad);


%% Calculate the time window
theta_interval = 1; %deg

[TimeInterval] = calulate_time_window(Time, colatitude_angle, theta_star_deg, theta_interval);
