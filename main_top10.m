clear
close all
clc

cd('C:\Users\admin\Desktop\GIT\PassiveSounding')
%addpath(genpath('mice'));
addpath(genpath('C:\Users\admin\Desktop\GIT\PassiveSounding\source'));

%% dir 
dir_in = 'out\Test7_SharadCandidates\';
addpath(genpath(dir_in));

dir_out = 'out\Test17_SharadCandidates\';
mkdir(dir_out);
addpath(genpath(dir_out));

fn_var = 'OutVariables.mat';

%% for each folder
folders = ls(dir_in);
folders(1:2,:)=[];

nb_folder = size(folders,1);

all_acquisitions =[];
for i_folder=1:nb_folder
   if exist([ dir_in strtrim(folders(i_folder,:)) '\' fn_var],'file')
       load([ dir_in strtrim(folders(i_folder,:)) '\' fn_var], 'num_rdr');
       if num_rdr~=0
           load([ dir_in strtrim(folders(i_folder,:)) '\' fn_var], 'TimeInterval_colatitude','SharadPassiveSoundingOpportunities');

           MinRotatedColat=[];
           for i_temp = 1:size(TimeInterval_colatitude,1)
                MinRotatedColat = [MinRotatedColat;    min(cell2mat(TimeInterval_colatitude(i_temp,3)))];
           end

           temp_tab = cell2table(TimeInterval_colatitude);
           temp_tab(:,1:2)=[];       temp_tab(:,2)=[];

           temp_top10 = [SharadPassiveSoundingOpportunities temp_tab array2table(MinRotatedColat)];

           all_acquisitions = [all_acquisitions; temp_top10];
        end
   else
       disp(['file out variable does not exists for dataset '  dir_in strtrim(folders(i_folder,:)) '\' fn_var]);
   end
end

%% analyse all acquisitions

MinRotatedColat = all_acquisitions.MinRotatedColat(:);

[sortedVals,indexes] = sort(MinRotatedColat);

top10 = all_acquisitions(indexes(1:10),:);

save([dir_out fn_var], 'top10','all_acquisitions');

top10(:,6)=[];
writetable(top10,'Top10_smallerRotColat.csv','Delimiter',';');
  
%% extract solarbusrt for all acquisition
cd(dir_out);

date_start = all_acquisitions.StartTime(:);
nb_entry = size(all_acquisitions,1);
Peaks=[];
for i_entry =1 : nb_entry
    date_tocheck = date_start(i_entry);
    formatOut = 'yyyymmdd';
    date_solar_burst=datestr(date_tocheck,formatOut);
    [nb_burst, solar_burst]= analyze_dynamic_specra_wind(date_solar_burst,0);
    if isempty(solar_burst)
        disp(['no burst in ' datestr(date_tocheck)])
        Peaks =[Peaks; 0]; 
    elseif size(solar_burst,2)==1
        Peaks =[Peaks; solar_burst.pks];    
    else% more peaks
        diff=[];
        for i_burst =1: size(solar_burst,2)
            diff = [diff; datestr(solar_burst(i_burst).time - date_tocheck, 'MM.SS')];
        end
        [~,i_burst]=min(str2double(diff));
        Peaks =[Peaks; solar_burst(i_burst).pks];
    end
end

all_acquisitions = [all_acquisitions array2table(Peaks)];

[sortedPeaks,indexesPeaks] = sort(Peaks,'descend');

top10_burst = all_acquisitions(indexesPeaks(1:10),:);

save(fn_var, 'top10','top10_burst','all_acquisitions');

top10_burst(:,6)=[];
writetable(top10_burst,'Top10_largerburst.csv','Delimiter',';');

all_acquisitions(:,6)=[];

writetable(all_acquisitions,'RotatedColat_Peak.csv','Delimiter',';');


cd('C:\Users\admin\Desktop\GIT\PassiveSounding')