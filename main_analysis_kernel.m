clc
clear
close all
cspice_kclear

addpath(genpath('mice'));
kernel_path = 'E:\Kernel\STEREO\stereo_a_spk_naif\STEREO-A_merged.bsp';
leapsecond_kernel_file = 'E:\Kernel\mro-psp1_2\lsk\naif0012.tls';
stereo_kernel_tm = 'E:\Kernel\STEREO\stereo_b_kernels.tm';

cspice_furnsh(stereo_kernel_tm);
cspice_furnsh(leapsecond_kernel_file);

timeWindow = cspice_spkcov( kernel_path, -234, 1e6 )



format = 'C';
prec=0;
timeWindow = cspice_et2utc( timeWindow', format, prec )

cspice_kclear