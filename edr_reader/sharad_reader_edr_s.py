#!/usr/bin/python
# Copyright (c) 2017.
#

# Author(s):
#   Sajid Pareeth <sajid.pareeth@fmach.it>
#   Nicola Gollin <nicola.gollin@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#    Make shure to use python3

import re
import csv
import argparse
import os
import numpy as np
import array
import scipy.io
from scipy.misc import imsave

# SHARAD RDR data (Italy) download here: http://pds-geosciences.wustl.edu/missions/mro/sharad.htm
# Header details given here: http://pds-geosciences.wustl.edu/mro/mro-m-sharad-4-rdr-v1/mrosh_1001/document/rdrsis.pdf
# All parameters are big endian >
def main():
  recorddimension=3786
  #usage = "usage: %prog [arguments]"
  parser = argparse.ArgumentParser(description='Read the Sharad EDR binary data. For a single Binary file, three files are created: 1) "data"_img.tiff - Image file 2) "data"_header.csv - Header data table 3) "data"_lbl.txt - Header parameters')
  parser.add_argument('-d', "--indata", dest="indata",
                 help='name of the binary file (.dat)')
  parser.add_argument('-i', "--indir", dest="indir",
                 help='Input directory (use . in case of current directory)')
  parser.add_argument('-o', "--outdir", dest="outdir",
                 help='Output directory (use . in case of current directory)')
  args = parser.parse_args()
  mindata = args.indata
  mindir = args.indir
  moutdir = args.outdir
  ##SETTING ABSOLUTE PATHS FOR INPUT & OUTPUT files
  out = '{indata}'.format(indata=mindata).split(".")[0]
  #print(*out)
  binary = os.path.abspath('{indir}/{indata}'.format(indir=mindir,indata=mindata))
  outimg = os.path.abspath('{outdir}/'.format(outdir=moutdir) + str(out) + '_img.tiff')
  outaux = os.path.abspath('{outdir}/'.format(outdir=moutdir) + str(out) + '_header.csv')
  outlbl = os.path.abspath('{outdir}/'.format(outdir=moutdir) + str(out) + '_lbl.txt')
  print("\n")
  fid = open(binary, "rb")
  # get number of bytes & records
  nmbrbytes = fid.seek(0,2)
  nmbrrecords = nmbrbytes/recorddimension
  # number of record check
  if nmbrbytes%recorddimension!=0:
    print("Wrong file or record dimension")
    return False
  print("DATA ANALYSIS:\ntotal # of bytes  ", nmbrbytes)
  print("total # of records", nmbrrecords,"\n")
  # Header definition
  raw_header = np.dtype([("ScetBlockWhole", ">u4"),
              ("ScetBlockFrac", ">u2"),
              ("TlmCounter", ">u4"),
              ("FmtLength", ">u2"),
              ("Spare1", ">u2"),
              ("ScetOstWhole", ">u4"),
              ("ScetOstFrac", ">u2"),
              ("Spare2", ">u1"),
              ("OstLineNumber", ">u1"),
              ("OstLine", ">u1",(16,)),
              ("Spare7", ">u1"),
              ("DataBlockId", ">u1",(3,)),
              ("ScienceDataSourceCounter", ">u2"),
              ("PacketSegmentationAndFpgaStatus", ">u2"),
              ("Spare10", ">u1"),
              ("DataBlockFirstPri", ">u1",(3,)),
              ("TimeDataBlockWhole", ">u4"),
              ("TimeDataBlockFrac", ">u2"),
              ("SdiBitField", ">u2"),
              ("TimeN", ">f4"),
              ("RadiusN", ">f4"),
              ("TangentialVelocityN", ">f4"),
              ("RadialVelocityN", ">f4"),
              ("Tlp", ">f4"),
              ("TimeWpf", ">f4"),
              ("DeltaTime", ">f4"),
              ("TlpInterpolate", ">f4"),
              ("RadiusInterpolate", ">f4"),
              ("TangentialVelocityInterpolate", ">f4"),
              ("RadialVelocityInterpolate", ">f4"),
              ("EndTlp", ">f4"),
              ("SCoeffs1", ">f4"),
              ("SCoeffs2", ">f4"),
              ("SCoeffs3", ">f4"),
              ("SCoeffs4", ">f4"),
              ("SCoeffs5", ">f4"),
              ("SCoeffs6", ">f4"),
              ("SCoeffs7", ">f4"),
              ("SCoeffs8", ">f4"),
              ("CCoeffs1", ">f4"),
              ("CCoeffs2", ">f4"),
              ("CCoeffs3", ">f4"),
              ("CCoeffs4", ">f4"),
              ("CCoeffs5", ">f4"),
              ("CCoeffs6", ">f4"),
              ("CCoeffs7", ">f4"),
              ("Slope", ">f4"),
              ("Topography", ">f4"),
              ("PhaseCompensationStep", ">f4"),
              ("ReceiveWindowOpeningTime", ">f4"),
              ("ReceiveWindowPosition", ">f4"),
              ("ScienceData", ">u1",(3600,))])

  header = np.dtype([("ScetBlockWhole", ">u4"),
              ("ScetBlockFrac", ">u2"),
              ("TlmCounter", ">u4"),
              ("FmtLength", ">u2"),
              ("Spare1", ">u2"),
              ("ScetOstWhole", ">u4"),
              ("ScetOstFrac", ">u2"),
              ("Spare2", ">u1"),
              ("OstLineNumber", ">u1"),
              ("PulseRepetitionInterval", "S5"),
              ("PhaseCompensationType", "S5"),
              ("Spare3", "S3"),
              ("DataTakeLength", "S13"),
              ("OperativeMode", "S9"),
              ("ManualGainControl", "S9"),
              ("CompressionSelection", "S1"),
              ("ClosedLoopTracking", "S1"),
              ("TrackingDataStorage", "S1"),
              ("TrackingPreSumming", "S4"),
              ("TrackingLogicSelection", "S1"),
              ("ThresholdLogicSelection", "S1"),
              ("SampleNumber", "S5"),
              ("Spare4", "S1"),
              ("AlphaBeta", "S3"),
              ("ReferenceBit", "S1"),
              ("Threshold", "S9"),
              ("ThresholdIncrement", "S9"),
              ("Spare5", "S5"),
              ("InitialEchoValue", "S4"),
              ("ExpectedEchoShift", "S4"),
              ("WindowLeftShift", "S4"),
              ("WindowRightShift", "S4"),
              ("Spare6", "S33"),
              ("Spare7", ">u1"),
              ("DataBlockId", ">u1",(3,)),
              ("ScienceDataSourceCounter", ">u2"),
              ("ScientificDataType", "S1"),
              ("SegmentationFlag", "S3"),
              ("Spare8", "S6"),
              ("Spare9", "S5"),
              ("DmaError", "S1"),
              ("TcOverrun", "S1"),
              ("FifoFull", "S1"),
              ("Test", "S1"),
              ("Spare10", ">u1"),
              ("DataBlockFirstPri", ">u1",(3,)),
              ("TimeDataBlockWhole", ">u4"),
              ("TimeDataBlockFrac", ">u2"),
              ("SdiBitField", ">u2"),
              ("TimeN", ">f4"),
              ("RadiusN", ">f4"),
              ("TangentialVelocityN", ">f4"),
              ("RadialVelocityN", ">f4"),
              ("Tlp", ">f4"),
              ("TimeWpf", ">f4"),
              ("DeltaTime", ">f4"),
              ("TlpInterpolate", ">f4"),
              ("RadiusInterpolate", ">f4"),
              ("TangentialVelocityInterpolate", ">f4"),
              ("RadialVelocityInterpolate", ">f4"),
              ("EndTlp", ">f4"),
              ("SCoeffs1", ">f4"),
              ("SCoeffs2", ">f4"),
              ("SCoeffs3", ">f4"),
              ("SCoeffs4", ">f4"),
              ("SCoeffs5", ">f4"),
              ("SCoeffs6", ">f4"),
              ("SCoeffs7", ">f4"),
              ("SCoeffs8", ">f4"),
              ("CCoeffs1", ">f4"),
              ("CCoeffs2", ">f4"),
              ("CCoeffs3", ">f4"),
              ("CCoeffs4", ">f4"),
              ("CCoeffs5", ">f4"),
              ("CCoeffs6", ">f4"),
              ("CCoeffs7", ">f4"),
              ("Slope", ">f4"),
              ("Topography", ">f4"),
              ("PhaseCompensationStep", ">f4"),
              ("ReceiveWindowOpeningTime", ">f4"),
              ("ReceiveWindowPosition", ">f4"),
              ("ScienceData", ">u1",(3600,))])


  head = np.fromfile(open(binary, "rb"), dtype=raw_header, count=-1, sep="")
  headers = np.empty(head.size, dtype=header)
  #SCoeffs and CCoefs are saved in this way since the csv output cause problems (goes in a new line in the middle of the field ("SCoeffs", ">f4",(8,)) for example)
  ##################################IMAGE##################

  Img = head["ScienceData"]
  Imgt = np.transpose(Img)
  scipy.io.savemat('{outdir}/'.format(outdir=moutdir) + str(out) + "_echoes.mat", mdict={str(out) + '_echoes': Imgt})

  ######################################LABELS###########

  Lbl = np.asarray(header.names)
  s1 = ';'.join(map(str, Lbl))

  ######################################CSV###########

  with open(outaux, "w", newline='') as f:
    writer = csv.writer(f, skipinitialspace = True)
    f.write(s1)
    f.write("\r")
    for x in range(0, head.size):
    #for x in range(0, 2):
      headers[x]["ScetBlockWhole"] = head[x]["ScetBlockWhole"]
      headers[x]["ScetBlockFrac"] = head[x]["ScetBlockFrac"]
      headers[x]["TlmCounter"] = head[x]["TlmCounter"]
      headers[x]["FmtLength"] = head[x]["FmtLength"]
      headers[x]["Spare1"] = head[x]["Spare1"]
      headers[x]["ScetOstWhole"] = head[x]["ScetOstWhole"]
      headers[x]["ScetOstFrac"] = head[x]["ScetOstFrac"]
      headers[x]["Spare2"] = head[x]["Spare2"]
      headers[x]["OstLineNumber"] = head[x]["OstLineNumber"]
      strip = (format(head[x]["OstLine"][0], '08b') + format(head[x]["OstLine"][1], '08b') + format(head[x]["OstLine"][2], '08b') +
               format(head[x]["OstLine"][3], '08b') + format(head[x]["OstLine"][4], '08b') + format(head[x]["OstLine"][5], '08b') +
               format(head[x]["OstLine"][6], '08b') + format(head[x]["OstLine"][7], '08b') + format(head[x]["OstLine"][8], '08b') +
               format(head[x]["OstLine"][9], '08b') + format(head[x]["OstLine"][10], '08b') + format(head[x]["OstLine"][11], '08b') +
               format(head[x]["OstLine"][11], '08b') + format(head[x]["OstLine"][12], '08b') + format(head[x]["OstLine"][13], '08b') +
               format(head[x]["OstLine"][14], '08b') + format(head[x]["OstLine"][15], '08b'))
      headers[x]["PulseRepetitionInterval"] = strip[0:4]
      headers[x]["PhaseCompensationType"] = strip[4:8]
      headers[x]["Spare3"] = strip[8:10]
      headers[x]["DataTakeLength"] = strip[10:33]
      headers[x]["OperativeMode"] = strip[33:41]
      headers[x]["ManualGainControl"] = strip[41:49]
      headers[x]["CompressionSelection"] = strip[49]
      headers[x]["ClosedLoopTracking"] = strip[50]
      headers[x]["TrackingDataStorage"] = strip[51]
      headers[x]["TrackingPreSumming"] = strip[52:55]
      headers[x]["TrackingLogicSelection"] = strip[55]
      headers[x]["ThresholdLogicSelection"] = strip[56]
      headers[x]["SampleNumber"] = strip[57:61]
      headers[x]["Spare4"] = strip[61]
      headers[x]["AlphaBeta"] = strip[62:64]
      headers[x]["ReferenceBit"] = strip[64]
      headers[x]["Threshold"] = strip[65:73]
      headers[x]["ThresholdIncrement"] = strip[73:81]
      headers[x]["Spare5"] = strip[81:85]
      headers[x]["InitialEchoValue"] = strip[85:88]
      headers[x]["ExpectedEchoShift"] = strip[88:91]
      headers[x]["WindowLeftShift"] = strip[91:94]
      headers[x]["WindowRightShift"] = strip[94:97]
      headers[x]["Spare6"] = strip[99:128]
      headers[x]["Spare7"] = head[x]["Spare7"]
      headers[x]["DataBlockId"] = head[x]["DataBlockId"]
      headers[x]["ScienceDataSourceCounter"] = head[x]["ScienceDataSourceCounter"]
      strip1 = format(head[x]["PacketSegmentationAndFpgaStatus"], '16b')
      headers[x]["ScientificDataType"] = strip1[0]
      headers[x]["SegmentationFlag"] = strip1[1:3]
      headers[x]["Spare8"] = strip1[3:8]
      headers[x]["Spare9"] = strip1[8:12]
      headers[x]["DmaError"] = strip1[12]
      headers[x]["TcOverrun"] = strip1[13]
      headers[x]["FifoFull"] = strip1[14]
      headers[x]["Test"] = strip1[15]
      headers[x]["Spare10"] = head[x]["Spare10"]
      headers[x]["DataBlockFirstPri"] = head[x]["DataBlockFirstPri"]
      headers[x]["TimeDataBlockWhole"] = head[x]["TimeDataBlockWhole"]
      headers[x]["TimeDataBlockFrac"] = head[x]["TimeDataBlockFrac"]
      headers[x]["SdiBitField"] = head[x]["SdiBitField"]
      headers[x]["TimeN"] = head[x]["TimeN"]
      headers[x]["RadiusN"] = head[x]["RadiusN"]
      headers[x]["TangentialVelocityN"] = head[x]["TangentialVelocityN"]
      headers[x]["RadialVelocityN"] = head[x]["RadialVelocityN"]
      headers[x]["Tlp"] = head[x]["Tlp"]
      headers[x]["TimeWpf"] = head[x]["TimeWpf"]
      headers[x]["DeltaTime"] = head[x]["DeltaTime"]
      headers[x]["TlpInterpolate"] = head[x]["TlpInterpolate"]
      headers[x]["RadiusInterpolate"] = head[x]["RadiusInterpolate"]
      headers[x]["TangentialVelocityInterpolate"] = head[x]["TangentialVelocityInterpolate"]
      headers[x]["RadialVelocityInterpolate"] = head[x]["RadialVelocityInterpolate"]
      headers[x]["EndTlp"] = head[x]["EndTlp"]
      headers[x]["SCoeffs1"] = head[x]["SCoeffs1"]
      headers[x]["SCoeffs2"] = head[x]["SCoeffs2"]
      headers[x]["SCoeffs3"] = head[x]["SCoeffs3"]
      headers[x]["SCoeffs4"] = head[x]["SCoeffs4"]
      headers[x]["SCoeffs5"] = head[x]["SCoeffs5"]
      headers[x]["SCoeffs6"] = head[x]["SCoeffs6"]
      headers[x]["SCoeffs7"] = head[x]["SCoeffs7"]
      headers[x]["SCoeffs8"] = head[x]["SCoeffs8"]
      headers[x]["CCoeffs1"] = head[x]["CCoeffs1"]
      headers[x]["CCoeffs2"] = head[x]["CCoeffs2"]
      headers[x]["CCoeffs3"] = head[x]["CCoeffs3"]
      headers[x]["CCoeffs4"] = head[x]["CCoeffs4"]
      headers[x]["CCoeffs5"] = head[x]["CCoeffs5"]
      headers[x]["CCoeffs6"] = head[x]["CCoeffs6"]
      headers[x]["CCoeffs7"] = head[x]["CCoeffs7"]
      headers[x]["Slope"] = head[x]["Slope"]
      headers[x]["Topography"] = head[x]["Topography"]
      headers[x]["PhaseCompensationStep"] = head[x]["PhaseCompensationStep"]
      headers[x]["ReceiveWindowOpeningTime"] = head[x]["ReceiveWindowOpeningTime"]
      headers[x]["ReceiveWindowPosition"] = head[x]["ReceiveWindowPosition"]

      hh = ';'.join(map(str, headers[x]))
      f.write(hh)
      f.write("\r")
    f.close()

  ######################################TXT###########

  s = '\r'.join(map(str, Lbl))
  #print(s) # properly display the label part

  with open(outlbl, "w") as f:
    f.write(s)
    f.close()

  print('\nFinished reading the EDR binary file: ','{indata}'.format(indata=mindata))
  print('Name of output files: \n', str(out) + '_echoes.tiff\n',str(out) + '_header.csv\n',str(out) + '_lbl.txt\n')
  print('Output files are saved here: ', os.path.abspath('{outdir}/'.format(outdir=moutdir)),"\n")
if __name__ == "__main__":
  main()
