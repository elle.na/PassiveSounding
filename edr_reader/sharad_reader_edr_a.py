#!/usr/bin/python
# Copyright (c) 2017.
#

# Author(s):
#   Sajid Pareeth <sajid.pareeth@fmach.it>
#   Nicola Gollin <nicola.gollin@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#    Make shure to use python3

import re
import csv
import argparse
import os
import numpy as np
import array

# SHARAD RDR data (Italy) download here: http://pds-geosciences.wustl.edu/missions/mro/sharad.htm
# Header details given here: http://pds-geosciences.wustl.edu/mro/mro-m-sharad-4-rdr-v1/mrosh_1001/document/rdrsis.pdf
# All parameters are big endian >
def main():
  recorddimension=267
  #usage = "usage: %prog [arguments]"
  parser = argparse.ArgumentParser(description='Read the Sharad EDR Auxiliary binary data. For a single Binary file, two files are created: 1) "data"_aux.csv - Header data table 2) "data"_lbl.txt - General header info')
  parser.add_argument('-d', "--indata", dest="indata",
                 help='name of the binary file (.dat)')
  parser.add_argument('-i', "--indir", dest="indir",
                 help='Input directory (use . in case of current directory)')
  parser.add_argument('-o', "--outdir", dest="outdir",
                 help='Output directory (use . in case of current directory)')
  args = parser.parse_args()
  mindata = args.indata
  mindir = args.indir
  moutdir = args.outdir
  ##SETTING ABSOLUTE PATHS FOR INPUT & OUTPUT files
  out = '{indata}'.format(indata=mindata).split(".")[0]
  #print(*out)
  binary = os.path.abspath('{indir}/{indata}'.format(indir=mindir,indata=mindata))
  outaux = os.path.abspath('{outdir}/'.format(outdir=moutdir) + str(out) + '_aux.csv')
  outlbl = os.path.abspath('{outdir}/'.format(outdir=moutdir) + str(out) + '_lbl.txt')
  
  print(binary)
  print("\n")
  fid = open(binary, "r+b")
  # get number of bytes & records
  nmbrbytes = fid.seek(0,2)
  nmbrrecords = nmbrbytes/recorddimension
  # number of record check
  if nmbrbytes%recorddimension!=0:
    print("Wrong file or record dimension")
    return False
  print("DATA ANALYSIS:\ntotal # of bytes  ", nmbrbytes)
  print("total # of records", nmbrrecords,"\n")
  # Header definition
  raw_header = np.dtype([("ScetBlockWhole", ">u4"),
  				   ("ScetBlockFrac", "<u2"),
  				   ("EphemerisTime", ">f8"),
  				   ("GeometryEpoch", ">u1", (23,)),
  				   ("SolarLongitude", ">f8"),
  				   ("OrbitNumber", ">u4"),
  				   ("XMarsScPositionVector", ">f8"),
                   ("YMarsScPositionVector", ">f8"),
                   ("ZMarsScPositionVector", ">f8"),
 				   ("SpacecraftAltitude", ">f8"),
 				   ("SubScEastLongitude", ">f8"),
  				   ("SubScPlanetocentricLatitude", ">f8"),
  				   ("SubScPlanetographicLatitude", ">f8"),
  				   ("XMarsScVelocityVector", ">f8"),
                   ("YMarsScVelocityVector", ">f8"),
                   ("ZMarsScVelocityVector", ">f8"),
  				   ("MarsScRadialVelocity", ">f8"),
  				   ("MarsScTangentialVelocity", ">f8"),
  				   ("LocalTrueSolarTime", ">f8"),
  				   ("SolarZenithAngle", ">f8"),
  				   ("ScPitchAngle", ">f8"),
  				   ("ScYawAngle", ">f8"),
  				   ("ScRollAngle", ">f8"),
  				   ("MroSamxInnerGimbalAngle", ">f8"),
  				   ("MroSamxOuterGimbalAngle", ">f8"),
  				   ("MroSapxInnerGimbalAngle", ">f8"),
  				   ("MroSapxOuterGimbalAngle", ">f8"),
  			       ("MroHgaInnerGimbalAngle", ">f8"),
  				   ("MroHgaOuterGimbalAngle", ">f8"),
  				   ("DesTemp", ">f4"),
  				   ("Des5v", ">f4"),
  				   ("Des12v", ">f4"),
  				   ("Des2v5", ">f4"),
  				   ("RxTemp", ">f4"),
  				   ("TxTemp", ">f4"),
  				   ("TxLev", ">f4"),
  				   ("TxCurr", ">f4"),
 				   ("CorruptedDataFlag", ">u2")])

  header = np.dtype([("ScetBlockWhole", ">u4"),
  				   ("ScetBlockFrac", "<u2"),
  				   ("EphemerisTime", ">f8"),
  				   ("GeometryEpoch", "str",23),
  				   ("SolarLongitude", ">f8"),
  				   ("OrbitNumber", ">u4"),
  				   ("XMarsScPositionVector", ">f8"),
                   ("YMarsScPositionVector", ">f8"),
                   ("ZMarsScPositionVector", ">f8"),
 				   ("SpacecraftAltitude", ">f8"),
 				   ("SubScEastLongitude", ">f8"),
  				   ("SubScPlanetocentricLatitude", ">f8"),
  				   ("SubScPlanetographicLatitude", ">f8"),
  				   ("XMarsScVelocityVector", ">f8"),
                   ("YMarsScVelocityVector", ">f8"),
                   ("ZMarsScVelocityVector", ">f8"),
  				   ("MarsScRadialVelocity", ">f8"),
  				   ("MarsScTangentialVelocity", ">f8"),
  				   ("LocalTrueSolarTime", ">f8"),
  				   ("SolarZenithAngle", ">f8"),
  				   ("ScPitchAngle", ">f8"),
  				   ("ScYawAngle", ">f8"),
  				   ("ScRollAngle", ">f8"),
  				   ("MroSamxInnerGimbalAngle", ">f8"),
  				   ("MroSamxOuterGimbalAngle", ">f8"),
  				   ("MroSapxInnerGimbalAngle", ">f8"),
  				   ("MroSapxOuterGimbalAngle", ">f8"),
  			       ("MroHgaInnerGimbalAngle", ">f8"),
  				   ("MroHgaOuterGimbalAngle", ">f8"),
  				   ("DesTemp", ">f4"),
  				   ("Des5v", ">f4"),
  				   ("Des12v", ">f4"),
  				   ("Des2v5", ">f4"),
  				   ("RxTemp", ">f4"),
  				   ("TxTemp", ">f4"),
  				   ("TxLev", ">f4"),
  				   ("TxCurr", ">f4"),
 				   ("CorruptedDataFlag", ">u2")])

  head = np.fromfile(open(binary, "rb"), dtype=raw_header, count=-1, sep="")
  headers = np.empty(head.size, dtype=header)

  ######################################LABELS###########

  Lbl = np.asarray(header.names)
  s1 = ';'.join(map(str, Lbl))

  ######################################CSV###########

  with open(outaux, "w", newline='') as f:
    writer = csv.writer(f, skipinitialspace = True)
    f.write(s1)
    f.write("\r")
    for x in range(0, head.size):

      headers[x]["ScetBlockWhole"] = head[x]["ScetBlockWhole"]
      headers[x]["ScetBlockFrac"] = head[x]["ScetBlockFrac"]
      headers[x]["EphemerisTime"] = head[x]["EphemerisTime"]
      headers[x]["GeometryEpoch"] = "".join([chr(item) for item in head[x]["GeometryEpoch"]])
      headers[x]["SolarLongitude"] = head[x]["SolarLongitude"]
      headers[x]["OrbitNumber"] = head[x]["OrbitNumber"]
      headers[x]["XMarsScPositionVector"] = head[x]["XMarsScPositionVector"]
      headers[x]["YMarsScPositionVector"] = head[x]["YMarsScPositionVector"]
      headers[x]["ZMarsScPositionVector"] = head[x]["ZMarsScPositionVector"]
      headers[x]["SpacecraftAltitude"] = head[x]["SpacecraftAltitude"]
      headers[x]["SubScEastLongitude"] = head[x]["SubScEastLongitude"]
      headers[x]["SubScPlanetocentricLatitude"] = head[x]["SubScPlanetocentricLatitude"]
      headers[x]["SubScPlanetographicLatitude"] = head[x]["SubScPlanetographicLatitude"]
      headers[x]["XMarsScVelocityVector"] = head[x]["XMarsScVelocityVector"]
      headers[x]["YMarsScVelocityVector"] = head[x]["YMarsScVelocityVector"]
      headers[x]["ZMarsScVelocityVector"] = head[x]["ZMarsScVelocityVector"]
      headers[x]["MarsScRadialVelocity"] = head[x]["MarsScRadialVelocity"]
      headers[x]["MarsScTangentialVelocity"] = head[x]["MarsScTangentialVelocity"]
      headers[x]["LocalTrueSolarTime"] = head[x]["LocalTrueSolarTime"]
      headers[x]["SolarZenithAngle"] = head[x]["SolarZenithAngle"]
      headers[x]["ScPitchAngle"] = head[x]["ScPitchAngle"]
      headers[x]["ScYawAngle"] = head[x]["ScYawAngle"]
      headers[x]["ScRollAngle"] = head[x]["ScRollAngle"]
      headers[x]["MroSamxInnerGimbalAngle"] = head[x]["MroSamxInnerGimbalAngle"]
      headers[x]["MroSamxOuterGimbalAngle"] = head[x]["MroSamxOuterGimbalAngle"]
      headers[x]["MroSapxInnerGimbalAngle"] = head[x]["MroSapxInnerGimbalAngle"]
      headers[x]["MroSapxOuterGimbalAngle"] = head[x]["MroSapxOuterGimbalAngle"]
      headers[x]["MroHgaInnerGimbalAngle"] = head[x]["MroHgaInnerGimbalAngle"]
      headers[x]["MroHgaOuterGimbalAngle"] = head[x]["MroHgaOuterGimbalAngle"]
      headers[x]["DesTemp"] = head[x]["DesTemp"]
      headers[x]["Des5v"] = head[x]["Des5v"]
      headers[x]["Des12v"] = head[x]["Des12v"]
      headers[x]["Des2v5"] = head[x]["Des2v5"]
      headers[x]["RxTemp"] = head[x]["RxTemp"]
      headers[x]["TxTemp"] = head[x]["TxTemp"]
      headers[x]["TxLev"] = head[x]["TxLev"]
      headers[x]["TxCurr"] = head[x]["TxCurr"]
      headers[x]["CorruptedDataFlag"] = head[x]["CorruptedDataFlag"]

      hh = ';'.join(map(str, headers[x]))
      f.write(hh)
      f.write("\r")
    f.close()

  ######################################TXT###########

  #Lbl comes from the LABELS section
  s = '\r'.join(map(str, Lbl))

  with open(outlbl, "w") as f:
    f.write(s)
    f.close()
  fid.close()

  #######################################GUI#########

  print('\nFinished reading the EDR Auxiliary binary file: ','{indata}'.format(indata=mindata))
  print('Name of output files: \n', str(out) + '_aux.csv\n',str(out) + '_lbl.txt\n')
  print('Output files are saved here: ', os.path.abspath('{outdir}/'.format(outdir=moutdir)),"\n")
if __name__ == "__main__":
  main()