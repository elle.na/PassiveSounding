clear
close all
clc

cd('C:\Users\admin\Desktop\GIT\PassiveSounding')
addpath(genpath('mice'));
addpath(genpath('C:\Users\admin\Desktop\GIT\PassiveSounding\source'));

kernel_path = 'D:\Kernel\';
addpath(genpath(kernel_path));

dir_path_csv = 'C:\Users\admin\Desktop\GIT\PassiveSounding\coordinate\';


fl_video = 0;
fl_plot = 1;
fl_rough_analysis =1;
%% load kernels

% to get the MRO kernels: wget -m -nH --cut-dirs=5 -nv https://naif.jpl.nasa.gov/pub/naif/pds/data/mro-m-spice-6-v1.0/mrosp_1000/
% Semenov, B.V., C.H. Acton, and M. Costa Sitja, MARS RECONNAISSANCE
% ORBITER SPICE KERNELS V1.0, MRO-M-SPICE-6-V1.0, NASA Planetary Data System, 2007. https://doi.org/10.17189/1520100

% generic kernels
% wget -m -nH --cut-dirs=5 -nv https://naif.jpl.nasa.gov/pub/naif/generic_kernels/

kernel_folder = [kernel_path 'mro-psp1_2\'];
mars_folder_kernel = [kernel_path 'MARS2020\kernels\'];
generic_kernel= [kernel_path 'generic_kernels\'];

addpath(genpath(kernel_folder));
addpath(genpath(mars_folder_kernel));

% set kernels
leapsecond_kernel_file = 'lsk\naif0012.tls';
fk_kernel_file = 'fk\mro_v16.tf';
% jupiter kernel
generic_kernel_jup = 'spk\satellites\jup365.bsp';
% mars kernel
mars_kernel_file ='spk\mar097.bsp';
% MRO kernel
sc_kernel_file = 'spk\mro_psp3.bsp';
% IAU_mars frame kernel
pck_mars_kernel = 'pck\pck00010.tpc';

% load kernels
cspice_furnsh([kernel_folder leapsecond_kernel_file]);
cspice_furnsh([generic_kernel generic_kernel_jup]);
cspice_furnsh([mars_folder_kernel pck_mars_kernel]);
cspice_furnsh([kernel_folder fk_kernel_file]);
cspice_furnsh([kernel_folder mars_kernel_file]);
cspice_furnsh([kernel_folder sc_kernel_file]);


%% set the time window of the sc from the kernels

fl_timeWindow_manual = 0;
if fl_timeWindow_manual ==1
    timeWindow_mro_UTC(1,:) = '2006 DEC 12 12:00:00';
    timeWindow_mro_UTC(2,:) = '2006 DEC 31 12:00:00';
    
    timeWindow_mro(1,:) = cspice_str2et( timeWindow_mro_UTC(1,:) );
    timeWindow_mro(2,:) = cspice_str2et( timeWindow_mro_UTC(2,:) );
else
    
    timeWindow_mro = cspice_spkcov( [kernel_folder sc_kernel_file], cspice_bodn2c('MRO'), 1e6 );
    format = 'C';
    prec=0;
    timeWindow_mro_UTC = cspice_et2utc( timeWindow_mro', format, prec );
    % size(timeWindow_mro) = [2,1]
    
    disp(timeWindow_mro_UTC(1,:))
    disp(timeWindow_mro_UTC(2,:))
end

%% calculate the state vectors
target_source   = 'JUPITER'; % JUPITER
target_sc   = 'MRO';
frame    = 'IAU_MARS';
abcorr   = 'LT+S';
observer = 'Mars';
time_interval = 10; % seconds

[diff_zx_rotated_deg,time_UTC,state_sc_sph,time_array]= calculate_state_vectors(target_source,target_sc,frame,abcorr,observer, time_interval,timeWindow_mro,fl_video);
close all

%% Load the data for one day
%load('data_one day.mat','theta_sc_star','diff_zx_rotated', 'time_UTC','time_interval')
%diff_zx_rotated_deg = diff_zx_rotated;

%% MRO time intervals
% find indeces with a proper colatitude angle --> idx_plot

% rough analysis
if fl_rough_analysis==1
    
    [idx_plt,val_idx_plt] = find_index_theta_star_rough(diff_zx_rotated_deg, time_UTC, time_interval,fl_plot);
    
else % finer analysis
    
    delta_theta_star = 10; % deg
    [idx_plt] = find_index_theta_star(state_sc_sph,observer, time_array, abcorr, target_sc,diff_zx_rotated_deg,delta_theta_star);
    val_idx_plt = diff_zx_rotated_deg(idx_plt);
end


close all

% find time intervals with a proper colatitude angle
[TimeInterval, start_time_mro, end_time_mro] = find_time_interval(idx_plt,diff_zx_rotated_deg,time_UTC);

%% SHARAD active time intervals
% find time intervals sharad is on

link_sharad = 'https://pds-geosciences.wustl.edu/mro/mro-m-sharad-3-edr-v1/mrosh_0001/';

% start and ending time column
start_time_column = 11;
end_time_column = 12;
link_column = 3;
nb_file=4;

path_data = 'data_sharad';
fn_data = 'cumindex_';

addpath(genpath(path_data));

[start_time_sharad, end_time_sharad, link_lbl] = extract_sharad_times(fn_data,path_data,nb_file,link_column, end_time_column,start_time_column);

[link_lbl_analysis,idx_indice_link,idx_plt_sharad,diff_zx_rotated_deg_idx_plt_sharad,start_time_sharad_analysis, end_time_sharad_analysis] =...
    find_index_colatitude_sharad(diff_zx_rotated_deg, time_UTC, time_interval,start_time_sharad, end_time_sharad,link_lbl);

% % % plot_agle_MRO_SHARAD(diff_zx_rotated_deg,idx_plt,val_idx_plt,fl_plot,time_UTC,start_time_sharad, end_time_sharad);


%% find common indeces bwt sharad and mro

[idx_common, i_sharad, i_mro] = intersect(idx_plt_sharad, idx_plt,'legacy');
ColatitudeCommon = diff_zx_rotated_deg(idx_common);
idx_link_sharad_mro = idx_indice_link(i_sharad); 

if fl_plot==1
    figure;
    
    plot(diff_zx_rotated_deg);
    hold on
    plot(idx_plt, val_idx_plt,'r.');
    
    hold on
    
    plot(idx_plt_sharad, diff_zx_rotated_deg_idx_plt_sharad,'y.');
    
    hold on

    plot(idx_common, ColatitudeCommon,'g.');

    hold off

    title(['Angular separation on the rotated zx plane (colatitude) from ' time_UTC{1} ' to ' time_UTC{end}]);
    xlabel(['Time with steps of ' num2str(time_interval) ' s']);
    ylabel('Colatitude in deg') 
    
    savefig('AngularSeparation_Sharad_MRO.fig')
end

[TimeInterval, start_time_Sharad_mro, end_time_Sharad_mro] = find_time_interval(idx_common,diff_zx_rotated_deg,time_UTC);


num_rdr = unique(idx_link_sharad_mro);

SharadLinkLBL = cell(length(num_rdr),1);

for i_rdr=1:length(num_rdr)
    idx_sharad_rdr = num_rdr(i_rdr);
    SharadLinkLBL(i_rdr)=link_lbl_analysis(idx_sharad_rdr);
end


savemat('OutVariables.mat','timeWindow_mro_UTC','num_rdr', 'SharadLinkLBL','TimeInterval', 'start_time_Sharad_mro', 'end_time_Sharad_mro')



return
%% Hierarchical intersection of the MRO data and the SHARAD active time

% questa parte non funziona davvero
% TO DO: va aggiornata estraendo gli indici da idx_plt
A = repmat(start_time_sharad,[1 length(start_time_mro)]);
[min_val,idx_rdr_sharad] = min(abs(A-start_time_mro'));

SHARAD_start_closestValue = start_time_sharad(idx_rdr_sharad);
SHARAD_end_closestValue = end_time_sharad(idx_rdr_sharad);
SHARAD_link_closestValue = link_lbl(idx_rdr_sharad);
clear A

%doens't really work becasue the length of acquisition window is different
diff_mro_sharad = abs(SHARAD_start_closestValue - start_time_mro);
th_time = hours;

[index_diff]=find(diff_mro_sharad < th_time);

idx_table=1;

length(index_diff)


% SharadLinkLBL=[];
% SharadStartTime=NaT; SharadEndTime=NaT;
% MROStartTime=NaT; MROEndTime=NaT;

idx_mro_ok=[];

for i_rdr = 1 : length(index_diff)
    idx_for = index_diff(i_rdr);
    
    if (SHARAD_start_closestValue(idx_for)< end_time_mro(idx_for) && ...
            SHARAD_start_closestValue(idx_for)> start_time_mro(idx_for)) ||...
            (SHARAD_end_closestValue(idx_for)< end_time_mro(idx_for) &&...
            SHARAD_end_closestValue(idx_for)> start_time_mro(idx_for))
        
        SharadStartTime(idx_table) = SHARAD_start_closestValue(idx_for);
        SharadEndTime(idx_table) = SHARAD_end_closestValue(idx_for);
        SharadLinkLBL(idx_table) = SHARAD_link_closestValue(idx_for);
        
        MROStartTime(idx_table) = start_time_mro(idx_for);
        MROEndTime(idx_table) = end_time_mro(idx_for);
        
        idx_mro_ok = [idx_mro_ok idx_for];
        
        idx_table = idx_table+1;
    end
    
end


%% save opportunities
RotatedColatitudeAngle = TimeInterval(idx_mro_ok,3);

% close all

colnames = {'SharadLinkLBL' 'SharadStartTime' 'SharadEndTime' 'MROStartTime' 'MROEndTime' 'RotatedColatitudeAngle'};
SharadPassiveSoundingOpportunities = table(lower(SharadLinkLBL'), SharadStartTime', SharadEndTime', MROStartTime', MROEndTime', RotatedColatitudeAngle, 'VariableNames', colnames);

SharadPassiveSoundingOpportunities;

save('SharadPassiveSoundingOpportunities.mat','SharadPassiveSoundingOpportunities')

cspice_kclear % unload kernels