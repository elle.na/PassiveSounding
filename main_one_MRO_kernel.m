clear
close all
clc

cd('C:\Users\admin\Desktop\GIT\PassiveSounding')
addpath(genpath('mice'));
addpath(genpath('C:\Users\admin\Desktop\GIT\PassiveSounding\source'));

%% dir out
dir_out = 'out\Test30_SharadCandidates_hf\';
addpath(genpath(dir_out));


%% kernels
kernel_path = 'E:\Kernel\';
addpath(genpath(kernel_path));


%kernel_mro_folder = [kernel_path 'mro-psp1_2\'];
kernel_mro_folder = [kernel_path 'mrosp_1000_151231_211231\mrosp_1000\data\'];
% mrosp_1000_070701_151231\mrosp_1000\data\
% mrosp_1000_151231_211231\mrosp_1000\data\
% D:\Kernel\mrosp_1000_070701_151231


% load kernels

% to get the MRO kernels: wget -m -nH --cut-dirs=5 -nv https://naif.jpl.nasa.gov/pub/naif/pds/data/mro-m-spice-6-v1.0/mrosp_1000/
% Semenov, B.V., C.H. Acton, and M. Costa Sitja, MARS RECONNAISSANCE
% ORBITER SPICE KERNELS V1.0, MRO-M-SPICE-6-V1.0, NASA Planetary Data System, 2007. https://doi.org/10.17189/1520100

% generic kernels
% wget -m -nH --cut-dirs=5 -nv https://naif.jpl.nasa.gov/pub/naif/generic_kernels/


mars_folder_kernel = [kernel_path 'MARS2020\kernels\'];
generic_kernel= [kernel_path 'generic_kernels\'];

addpath(genpath(kernel_mro_folder));
addpath(genpath(mars_folder_kernel));

% set kernels
leapsecond_kernel_file = 'lsk\naif0012.tls';
fk_kernel_file = 'fk\mro_v16.tf';
% jupiter kernel
generic_kernel_jup = 'spk\satellites\jup365.bsp';
% mars kernel
mars_kernel_file ='spk\mar097.bsp';
% MRO kernel
%sc_kernel_file = ['spk\' fn_mro_kernel];
% sc_kernel_file = 'spk\mro_psp3.bsp';
% IAU_mars frame kernel
pck_mars_kernel = 'pck\pck00010.tpc';
% gravity kernel
gravity_kernel='pck\gm_de431.tpc';

% load kernels
cspice_furnsh([kernel_mro_folder leapsecond_kernel_file]);
cspice_furnsh([generic_kernel generic_kernel_jup]);
cspice_furnsh([generic_kernel gravity_kernel]);
cspice_furnsh([mars_folder_kernel pck_mars_kernel]);
cspice_furnsh([kernel_mro_folder fk_kernel_file]);
cspice_furnsh([kernel_mro_folder mars_kernel_file]);
%cspice_furnsh([kernel_mro_folder sc_kernel_file]);


%% per each mro kernel
mro_bsp_files = dir([kernel_mro_folder '\spk\mro_psp57.bsp']);

n_files = size(mro_bsp_files,1);
n_rdr_tot=0;

   
fn_mro_kernel = mro_bsp_files.name;

cspice_furnsh([kernel_mro_folder '\spk\' fn_mro_kernel]);

%% start and ending time
timeWindow_mro = cspice_spkcov( [kernel_mro_folder 'spk\' fn_mro_kernel], cspice_bodn2c('MRO'), 1e6 );
format = 'C';
prec=0;
timeWindow_mro_UTC = cspice_et2utc( timeWindow_mro', format, prec );
% size(timeWindow_mro) = [2,1]

disp('-------------------------------------------------')
disp(fn_mro_kernel);
disp(timeWindow_mro_UTC(1,:))
disp(timeWindow_mro_UTC(2,:))

%% dir out
fn_outdir = [timeWindow_mro_UTC(1,1:8) '-' timeWindow_mro_UTC(2,1:8)];
fn_outdir(5)=[];
fn_outdir(13)=[];

outdir = [dir_out fn_outdir];


mkdir(outdir);
cd(outdir);

%% run main

delta_theta_star = 6; % deg

[timeWindow_mro_UTC,num_rdr, ~,~,n_rdr_tot]=...
extract_lbl_list([kernel_mro_folder 'spk\' fn_mro_kernel],delta_theta_star,n_rdr_tot);
close all


%% post processing
cd('C:\Users\admin\Desktop\GIT\PassiveSounding')
close all
    


disp(['Total number of rdr ' num2str(n_rdr_tot)])
disp('DONE')
clear