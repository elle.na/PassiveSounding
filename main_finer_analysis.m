clc
clear 
close all

addpath('edr_reader') 
addpath('edr_sharad') 

link_sharad = 'https://pds-geosciences.wustl.edu/mro/mro-m-sharad-3-edr-v1/mrosh_0001/';
load('SharadPassiveSoundingOpportunities.mat', 'SharadPassiveSoundingOpportunities')

[nb_rdr,~] = size(SharadPassiveSoundingOpportunities);

for i_rdr = 1 : nb_rdr
    
    % find links
    link_rdr = cell2mat(SharadPassiveSoundingOpportunities.Var1(i_rdr));
    link_lbl = [link_sharad link_rdr];
    
    newStr1 = split(link_rdr,'/');
    fn_lbl = cell2mat(newStr1(end));
    
    newStr2 = split(fn_lbl,'.');
    fn_rdr = cell2mat(newStr2(1));
    
    fn_edr_aux = [fn_rdr '_a.dat'];
    link_edr_aux = [link_sharad cell2mat(newStr1(1)) '/' cell2mat(newStr1(2)) '/' cell2mat(newStr1(3)) '/' fn_edr_aux];
    
    % data download
    mkdir(['edr_sharad/' fn_rdr])
    websave(['edr_sharad/' fn_rdr '/' fn_lbl], link_lbl);
    websave(['edr_sharad/' fn_rdr '/' fn_edr_aux], link_edr_aux);
    
    % python reading
    cd('edr_reader') 
    system(['python3 sharad_reader_edr_a.py -d ' fn_edr_aux ' -i ../edr_sharad/' fn_rdr ' -o ../edr_sharad/' fn_rdr]); 
    % python3 sharad_reader_edr_a.py -d e_0177201_001_ss19_700_a_a.dat -i ../edr_sharad/e_0177201_001_ss19_700_a -o ../edr_sharad/e_0177201_001_ss19_700_a
    cd ..
    
    % load data
    fn_table = ['edr_sharad/' fn_rdr '/' fn_rdr '_a_aux.csv'];
    opts = detectImportOptions(fn_table, 'Delimiter', ';','VariableNamingRule','preserve');
    table_sharad = readtable(fn_table,opts);
    clear opts
    
    % altitude stats
    SC_Altitude = table_sharad.SpacecraftAltitude;
    mean_SC_altitude = mean(SC_Altitude);
    std_SC_altitude = std(SC_Altitude);
    
    
    
    
    
end